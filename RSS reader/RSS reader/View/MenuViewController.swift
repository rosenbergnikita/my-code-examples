//
//  MenuViewController.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit
import SideMenu



class MenuViewController: UIViewController,
UITableViewDataSource,
UITableViewDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    
    
    //MARK: - Properties
    private var channels = [Channel]()
    private var combinedMode = false
    private var modeCell: ShowTypeCell?
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        self.channels = DATA.articlesService.chanells()
        self.tableView.reloadData()
    }
    
    
    
    
    //MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
            
        default:
            return self.channels.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let c = tableView.dequeueReusableCell(withIdentifier: "\(ChannelCell.self)") as! ChannelCell
                c.channel = nil
                return c
                
            case 1:
                let c = tableView.dequeueReusableCell(withIdentifier: "\(ShowTypeCell.self)") as! ShowTypeCell
                c.combinedSwitch.setOn(self.combinedMode, animated: false)
                c.combinedSwitch.addTarget(self, action: #selector(type(of: self).combinedModeChangedAction(_:)), for: .valueChanged, removePrevious: true)
                self.modeCell = c
                return c
                
            default:
                break
            }
          
            
        case 1:
            let c = tableView.dequeueReusableCell(withIdentifier: "\(ChannelCell.self)") as! ChannelCell
            c.channel = self.channels[indexPath.row]
            c.accessoryType = self.combinedMode ? .none : .disclosureIndicator
            return c
            
            
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return nil
            
        case 1:
            return "Channels"
            
        default:
            return nil
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 && indexPath.row == 0 {
            self.addChannelAction()
        }
        else if !self.combinedMode {
            let channel = self.channels[indexPath.row]
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    

    
    //MARK: - Actions
    func addChannelAction() {
        let a = UIAlertController(title: "Please enter feed URL", message: nil, preferredStyle: .alert)
        a.addTextField { (tf) in
            
        }
        a.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            guard let text = a.textFields?.first?.text, !text.isEmpty else {
                return
            }
            self.addChannel(withLink: text)
        }))
        a.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
        self.present(a, animated: true, completion: nil)
    }
    
    
    func combinedModeChangedAction(_ sender: UISwitch) {
        self.combinedMode = sender.isOn
        if self.combinedMode {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.dismiss(animated: true, completion: nil)
            })
        }
        else {
            self.tableView.reloadData()
        }
    }
    
    
    
    
    //MARK: - Routines
    private func addChannel(withLink: String) {
        
    }
}
