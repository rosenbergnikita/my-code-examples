//
//  ArticleCell.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit


class ArticleCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    
    
    //MARK: - Properties
    var data: Article? {
        didSet {
            title.text = data?.title
            desc.text = data?.desc
        }
    }
}
