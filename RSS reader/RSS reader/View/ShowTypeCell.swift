//
//  ShowTypeCell.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit


class ShowTypeCell: UITableViewCell {
    
    @IBOutlet weak var combinedSwitch: UISwitch!
}
