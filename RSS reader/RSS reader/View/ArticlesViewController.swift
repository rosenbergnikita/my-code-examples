//
//  ArticlesViewController.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit

class ArticlesViewController: UITableViewController {
    
    //MARK: - Outlets

    
    //MARK: - Properties
    private var articles = [Article]()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
        
        
        let channel = DATA.articlesService.chanells().first!
        DATA.articlesService.articles(in: channel) { (r) in
            self.articles = r
            self.tableView.reloadData()
        }
    }

    
    
    
    //MARK: - Table View
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "\(ArticleCell.self)") as! ArticleCell
        c.data = self.articles[indexPath.row]
        
        return c
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let a = self.articles[indexPath.row]
        if let url = a.link?.url {
            UIApplication.shared.openURL(url)
        }
    }
}

