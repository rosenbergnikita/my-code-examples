//
//  ChannelCell.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit
import SDWebImage



class ChannelCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    
    //MARK: - Properties
    var channel: Channel? {
        didSet {
            picture.image = #imageLiteral(resourceName: "rss")
            if channel == nil {
                title.text = "Add"
            }
            else {
                if let url = channel!.imageLink?.url {
                    picture.sd_setImage(with: url, placeholderImage: self.picture.image)
                }
                title.text = channel!.name
            }
        }
    }
}



