//
//  CollectionExtension.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/8/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation
import Swift



extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index?) -> Iterator.Element? {
        guard let i = index else {
            return nil
        }
        
        //TODO: check this, may crash
        return i >= self.startIndex && i < self.endIndex ? self[i] : nil
    }
    
    
    

 
}
