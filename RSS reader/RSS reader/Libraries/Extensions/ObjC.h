//
//  ObjC.h
//  HotelFace
//
//  Created by Nikita Rosenberg on 6/13/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjC : NSObject

+ (BOOL)catchException:(void(^)())tryBlock error:(__autoreleasing NSError **)error;

@end
