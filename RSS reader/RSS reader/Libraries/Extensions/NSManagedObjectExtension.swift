//
//  NSManagedObjectExtension.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import CoreData


extension NSManagedObject {
    
    static func deleteObjects(from set: NSSet?) {
        guard let objects = set?.allObjects as? [NSManagedObject] else {
            return
        }
        
        for o in objects {
            o.mr_deleteEntity()
        }
    }
    
}
