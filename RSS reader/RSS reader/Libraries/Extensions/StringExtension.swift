//
//  StringExtension.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 6/13/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation
import UIKit



extension String {
    
    var url: URL? {
        if !self.isEmpty {
            if let url = URL(string: self) {
                return url
            }
        }
        return nil
    }
    
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
}
