//
//  ArrayExtension.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 12/29/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation
import Swift



extension Array {
    
    
    public mutating func appendIfNoElementWithSuchClass(_ newElement: Iterator.Element) {
        let exist = self.filter { (e: Element) -> Bool in
            return type(of: e) == type(of: newElement)
        }
        
        if exist.count == 0 {
            self.append(newElement)
        }
    }
    
}
