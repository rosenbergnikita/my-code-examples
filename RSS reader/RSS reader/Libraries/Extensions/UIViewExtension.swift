//
//  UIViewExtension.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 9/28/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import UIKit
import ObjectiveC



private var borderLeftKey: UInt8 = 0




extension UIView {
    
    var firstResponder: Any? {
        if self.isFirstResponder {
            return self
        }
        else {
            for v in self.subviews {
                if let r = v.firstResponder {
                    return r
                }
            }
        }
        
        return nil
    }
    
    
    func round(_ round: Bool = true, borderWidth: Float = 0, _ borderColor: UIColor = UIColor.clear) {
        self.layer.masksToBounds = true
        self.layer.borderWidth = round ? CGFloat(borderWidth) : 0
        self.layer.borderColor = round ? borderColor.cgColor : UIColor.clear.cgColor
        self.layer.cornerRadius = round ? self.frame.width/2 : 0
    }
    
    
    func removeAllGestureRecongnizers() {
        for r in self.gestureRecognizers ?? [] {
            self.removeGestureRecognizer(r)
        }
    }
    
    
    func removeAllSubviews() {
        for s in self.subviews {
            s.removeFromSuperview()
        }
    }
    

    
    private var borderLeft: CALayer? {
        get {
            return objc_getAssociatedObject(self, &borderLeftKey) as? CALayer
        }
        set(newValue) {
            objc_setAssociatedObject(self, &borderLeftKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    
    func addBorderLeft(color: UIColor = UIColor.lightGray, width: CGFloat = 0.5) {
        self.borderLeft?.removeFromSuperlayer()
        
        self.borderLeft = CALayer()
        self.borderLeft!.borderColor = color.cgColor
        self.borderLeft!.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.borderLeft!.borderWidth = width
        self.layer.addSublayer(self.borderLeft!)
    }
 
}
