//
//  DataManager.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//


import Foundation
import CoreData
import MagicalRecord



let DATA = DataController.shared




class DataController: NSObject  {
    
    //MARK: Lifecycle
    static let shared = DataController()
    private override init() {
        super.init()
        
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: "rss.sqlite")
        self.seedCoreData()
    }
    
    
    
    
    //MARK: - Convenience
    //Object for user independent data
    lazy var persistent: Persistent = {
        let result = Persistent.mr_findFirstOrCreate(byAttribute: "persistentID", withValue: -1)
        return result
    }()
    
    

    func save () {
        MagicalRecord.save({ (context) in
            
        })
    }
    
    
    
    
    // MARK: - Services
    lazy var articlesService: ArticlesService = {
        return ArticlesService(data: self)
    }()
   
    
    
    

    

    
    
    //MARK: - Seed
    private func seedCoreData() {
        guard self.persistent.isFirstAppSession() else {
            return
        }
        
        guard let path = Bundle.main.path(forResource: "SeedChannels", ofType: "plist") else {
            return
        }
        
        guard let array = NSArray(contentsOfFile: path) as? [[String: String]] else {
            return
        }
        
        for item in array {
            let c = Channel.mr_createEntity()
            c?.name = item["name"]
            c?.link = item["link"]
        }
    }
}
