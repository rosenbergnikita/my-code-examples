//
//  Persistent+Extension.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import CoreData





extension Persistent {
    
    
    
    
    
    static private var firstAppSession = false
    func isFirstAppSession() -> Bool {
        //Check persistent var
        if self.firstLaunch {
            self.firstLaunch = false
            type(of: self).firstAppSession = true
            return true
        }
        else {
            return type(of: self).firstAppSession
        }
    }
    

}
