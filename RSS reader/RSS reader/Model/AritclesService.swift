//
//  RssService.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import Foundation
import FeedKit
import CoreData


class ArticlesService {
    
    
    //MARK: - Lifecycle
    private weak var data: DataController?
    init(data: DataController) {
        self.data = data
    }
    
    
    //MARK: - Interface
    func chanells() -> [Channel] {
        return Channel.mr_findAllSorted(by: "name", ascending: true) as! [Channel]
    }
    
    
    func add(channel: Channel) {
        
    }
    
    
    func delete(channel: Channel) {
        
    }
    
    
    
    
    func artciles(_ result: @escaping ([Article])->()) {
        result([])
    }
    
    
    func articles(in channels: [Channel], _ result: @escaping ([Article])->()) {
        result([])
    }
    
    
    func articles(in channel: Channel, _ result: @escaping ([Article])->()) {
        guard let url = channel.link?.url else {
            result([])
            return
        }
        
        guard let parser = FeedParser(URL: url) else {
            result([])
            return
        }
        
        parser.parse { (parsingResult) in
            var articles = [Article]()
            
            if let f = parsingResult.rssFeed {
                articles.append(contentsOf: self.convertFeedToCoreData(feed: f, channel: channel))
            }
            else if let f = parsingResult.atomFeed {
                articles.append(contentsOf: self.convertFeedToCoreData(feed: f, channel: channel))
            }
            else if let error = parsingResult.error {
                
            }
            
            result(articles)
        }
    }
    
    
    
    
    //MARK: - Routines
    private func convertFeedToCoreData(feed: AnyObject, channel: Channel) -> [Article] {
        

        
        NSManagedObject.deleteObjects(from: channel.articles)
        var result: [Article] = []
        
        if let rss = feed as? RSSFeed {
            for item in rss.items ?? [] {
                guard let a = Article.mr_createEntity() else {
                    continue
                }
                
                a.title = item.title
                a.link = item.link
                a.desc = item.description?.trim()
                if let ti = item.pubDate?.timeIntervalSinceReferenceDate {
                    a.published = NSDate(timeIntervalSinceReferenceDate: ti)
                }
                a.channel = channel
                result.append(a)
            }
        }
        
        
    
        return result
    }
}
