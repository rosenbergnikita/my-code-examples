//
//  Settings.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import Foundation
import NSLogger
import MagicalRecord
//import AlamofireNetworkActivityIndicator



let SETTINGS = Settings.shared


class Settings {
    
    //MARK: - Lifecycle
    static let shared = Settings()
    private init() {
        
    }
    
    
    
    
    //MARK: - Properties
    enum RunMode {
        case unknown
        case debug //Device connected to mac, simulator
        case stage //Fabric
        case release //AppStore, TestFlight testing
    }
    
    //TODO: implement mode defines
    var runMode: RunMode {
        return .debug
        /*
         #if RELEASE_MODE
         return .release
         #elseif DEBUG_MODE
         return .debug
         #elseif STAGE_MODE
         return .stage
         #else
         return .unknown
         #endif
         */
    }
    
    let remoteLogHost = "rosenbergnikita.hopto.org"
    let remoteLogPort = 50000
    
    
  
    
    
    //MARK: - Routines
    func frameworksSetup(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        self.loggerSetup()
        
        
        
        MagicalRecord.enableShorthandMethods()
    }
    
    
    private func loggerSetup() {
        let environment = ProcessInfo.processInfo.environment
        let debuggerAttached = environment["debugger_attached"] == "true"
        if SETTINGS.runMode == .stage {
            LoggerSetViewerHost(LoggerGetDefaultLogger(), SETTINGS.remoteLogHost as CFString!, UInt32(SETTINGS.remoteLogPort))
        }
        else if SETTINGS.runMode == .debug && !debuggerAttached {
            LoggerSetOptions(LoggerGetDefaultLogger(), UInt32(kLoggerOption_BrowseBonjour))
        }
        else {
            LoggerSetOptions(LoggerGetDefaultLogger(), UInt32(kLoggerOption_LogToConsole))
        }
        
        LoggerStart(LoggerGetDefaultLogger())
    }
    
    
}
