//
//  NSLoggerSupport.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 6/22/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation
import NSLogger


enum LogLevel: Int32 {
    case info = 0
    case warning = 1
    case error = 2
}
typealias LL = LogLevel


enum LogDomain: String {
    case Global
    case ViewControllerAppearance
}


//Support variadic parameters functions for swift
func Log(_ from: Any?, level: LogLevel, message: String!, args: CVarArg...) {
    var domain: String!
    if from == nil {
        domain = LogDomain.Global.rawValue
    }
    else {
        if let d = from as? String {
            domain = d
        }
        else {
            domain = "\(type(of: from!))"
        }
    }
    LogMessage_va(domain, level.rawValue, message, getVaList(args))
}

