//
//  AppDelegate.swift
//  RSS reader
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit
import NSLogger




@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    //MARK: - Properties
    var window: UIWindow?
    
    
    //MARK: - Delegate
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        SETTINGS.frameworksSetup(launchOptions: launchOptions)
        self.showLoadingUI()
        
        return true
    }
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return false
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        Log(self, level: .info, message: "applicationWillResignActive")
        DATA.save()
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Log(self, level: .info, message: "applicationDidEnterBackground")
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        Log(self, level: .info, message: "applicationWillEnterForeground")
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Log(self, level: .info, message: "applicationDidBecomeActive")
        application.applicationIconBadgeNumber = 0
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        Log(self, level: .info, message: "applicationWillTerminate")
        DATA.persistent.firstLaunch = false
        DATA.save()
        LoggerFlush(LoggerGetDefaultLogger(), false)
    }
    
    
    
    //MARK: - UI
    private func showLoadingUI() {
        self.loadingFinished()
    }
    
    
    func loadingFinished() {
        self.showNormalUI()
    }
    
    
    private func showNormalUI() {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() {
            self.showWindowWithRoot(vc)
        }
    }
    
    
    private func showWindowWithRoot(_ root: UIViewController) {
        if self.window == nil {
            self.window = UIWindow(frame: UIScreen.main.bounds)
        }
        self.window!.backgroundColor = UIColor.white
        self.window!.rootViewController = root
        self.window!.makeKeyAndVisible()
    }
    
    
    
}
