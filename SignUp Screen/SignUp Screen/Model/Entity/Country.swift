//
//  Country.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/8/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import Foundation


extension Country {
    
    
    var fullName: String {
        var result = self.name ?? ""
        if let p = self.prefix, !p.isEmpty {
            result = "\(p) \(result)"
        }
        
        return result
    }
    
}
