//
//  DataManager.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/8/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import Foundation
import MagicalRecord
import CSV



class DataManager {
    
    
    //MARK: - Lifecycle

    static var shared = DataManager()
    private init() {
        MagicalRecord.enableShorthandMethods()
        //MagicalRecord.setDefaultModelNamed("Model")
        MagicalRecord.setShouldDeleteStoreOnModelMismatch(true)
        MagicalRecord.setShouldAutoCreateManagedObjectModel(true)
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: "SignUpScreen")
    }
    
    
    deinit {
        MagicalRecord.cleanUp()
    }
    
    
    
    func save() {
        MagicalRecord.save(blockAndWait: {_ in })
    }
    
    
    
    
    
    //MARK: - Countries
    func countries(_ resultBlock: @escaping ([Country]) -> ()) {
        objc_sync_enter(self)
        defer {
            objc_sync_exit(self)
        }
        
        
        
        var result = self.countries()
        if result.isEmpty {
            DispatchQueue.main.async {
                self.loadCountriesFromCSV()
                result = self.countries()
                resultBlock(result)
            }
        }
        else {
            resultBlock(result)
        }
    }
        

    private func countries() -> [Country] {
        return Country.mr_findAllSorted(by: "name", ascending: true) as? [Country] ?? []
    }
    
    
    private func loadCountriesFromCSV() {
        guard let url = Bundle.main.url(forResource: "list_of_countries", withExtension: "csv") else {
            return
        }
        
        guard let stream = InputStream(url: url) else {
            return
        }
        
        guard let csv = try? CSV(stream: stream) else {
            return
        }
        
        Country.mr_truncateAll()
        for row in csv  {
            let c = Country.mr_createEntity()!
            c.name = row.first?.trim()
            c.prefix = row[safe: 1]?.trim()
        }
        self.save()
    }
}
