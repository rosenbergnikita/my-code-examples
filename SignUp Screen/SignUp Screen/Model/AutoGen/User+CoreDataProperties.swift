//
//  User+CoreDataProperties.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/8/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var birthday: NSDate?
    @NSManaged public var email: String?
    @NSManaged public var login: String?
    @NSManaged public var password: String?
    @NSManaged public var userID: Int32
    @NSManaged public var password2Transient: String?
    @NSManaged public var country: Country?

}
