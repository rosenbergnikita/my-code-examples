//
//  AppDelegate.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/8/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit
import CoreData
import MagicalRecord



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        _ = DataManager.shared
        return true
    }

   
    func applicationWillTerminate(_ application: UIApplication) {
        DataManager.shared.save()
    }

}

