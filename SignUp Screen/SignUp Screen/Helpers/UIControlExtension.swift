//
//  UIControlExtension.swift
//
//  Created by Nikita Rosenberg on 3/25/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit



extension UIControl {
    
    open func replaceTarget(_ target: Any?, action: Selector, for controlEvents: UIControlEvents) {
        self.removeTarget(target, action: action, for: controlEvents)
        self.addTarget(target, action: action, for: controlEvents)
    }
    
}
