//
//  CollectionExtension.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/8/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import Foundation


extension Collection where Indices.Iterator.Element == Index {
    
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Generator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
