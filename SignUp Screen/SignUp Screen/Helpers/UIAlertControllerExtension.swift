//
//  UIAlertControllerExtension.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/9/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit



extension UIAlertController {
    
    static func alert(_ message: String?, from: UIViewController) {
        self.alert(title: nil, message: message, from: from)
    }
    
    
    static func alert(title: String?, message: String?, from: UIViewController) {
        let a = UIAlertController(title: title, message: message, preferredStyle: .alert)
        a.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        from.present(a, animated: true, completion: nil)
    }
    
    
}
