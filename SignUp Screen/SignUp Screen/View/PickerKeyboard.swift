//
//  PickerKeyboard.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/9/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit



class PickerKeyboard: NSObject,
UIPickerViewDelegate {
    
    
    //MARK: - Outlets
    @IBOutlet var picker: UIDatePicker!
    @IBOutlet var toolbar: UIToolbar!
    weak var assignedTextField: UITextField? {
        willSet {
            assignedTextField?.inputView = nil
            assignedTextField?.inputAccessoryView = nil
        }
        didSet {
            assignedTextField?.inputView = picker
            assignedTextField?.inputAccessoryView = toolbar
            if let date = self.dateFormatter.date(from: assignedTextField?.text ?? "") {
                self.picker.date = date
            }
        }
    }
    
    
    //MARK: - Properties
    var dateFormatter: DateFormatter
    
    
    
    //MARK: - Lifecycle
    init(dateFormatter: DateFormatter) {
        self.dateFormatter = dateFormatter
        super.init()
        
        let views = Bundle.main.loadNibNamed("\(PickerKeyboard.self)", owner: self, options: nil)
        self.picker = views?.first as! UIDatePicker
        self.toolbar = views?[safe: 1] as! UIToolbar
        self.configureView()
    }
    
    
    private func configureView() {
        
        
    }
    
    
    
    

    
    
    //MARK: - Actions
    @IBAction func clearAction(_ sender: Any) {
        self.assignedTextField?.text = nil
        self.assignedTextField?.resignFirstResponder()
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        let date = self.picker.date
        self.assignedTextField?.text = self.dateFormatter.string(from: date as Date)
        self.assignedTextField?.resignFirstResponder()
    }
}
