//
//  SignUpCell.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/8/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit





class SignUpCell: UITableViewCell {
    
    enum CellType {
        case login
        case password
        case password2
        case birthday
        case country
        case email
        case unknown
    }
    
    
    
    //MARK: - Outlets
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var textField: UITextField!
    
    
    //MARK: - Properties
    
    var dateFormatter: DateFormatter!
    
    var type = CellType.unknown
    var data: User! {
        didSet {
            
            self.textField.text = nil
            self.icon.image = nil
            self.textField.isSecureTextEntry = false
            self.textField.returnKeyType = .next
            self.textField.keyboardType = .default
            self.textField.placeholder = nil
            
            switch self.type {
            case .login:
                self.textField.text = data.login
                self.icon.image = #imageLiteral(resourceName: "ic_login")
                self.textField.placeholder = "Login"
                
            case .password:
                self.textField.text = data.password
                self.icon.image = #imageLiteral(resourceName: "ic_password")
                self.textField.isSecureTextEntry = true
                self.textField.placeholder = "Password"
                
            case .password2:
                self.textField.text = data.password2Transient
                self.textField.isSecureTextEntry = true
                self.textField.placeholder = "Repeat password"
                
            case .birthday:
                if let date = data.birthday as Date? {
                    self.textField.text = self.dateFormatter.string(from: date)
                }
                self.icon.image = #imageLiteral(resourceName: "calendar")
                self.textField.placeholder = "Birthday"
                
            case .country:
                self.textField.text = data.country?.fullName
                self.icon.image = #imageLiteral(resourceName: "ic_countries")
                self.textField.placeholder = "Country"
                
            case .email:
                self.textField.text = data.email
                self.icon.image = #imageLiteral(resourceName: "ic_mail")
                self.textField.keyboardType = .emailAddress
                self.textField.placeholder = "Email"
                
            default:
                break
            }
        }
    }
    
    
}
