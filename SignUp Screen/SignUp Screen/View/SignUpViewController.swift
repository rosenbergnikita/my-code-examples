//
//  SignUpViewController.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/8/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding



class SignUpViewController: UIViewController,
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate,
SelectCountryOutput,
SignUpBottomCellOutput {
    
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: TPKeyboardAvoidingTableView!
    private var datePicker: PickerKeyboard!
    
    
    //MARK: - Properties
    private var cellsTypes: [SignUpCell.CellType] = [.login,
                                               .password,
                                               .password2,
                                               .birthday,
                                               .country,
                                               .email]
    private var user: User!
    private var termsAgreement = false
    let dateFormatter: DateFormatter = {
        let r = DateFormatter()
        r.dateFormat = "dd.MM.yyyy"
        return r
    }()
    
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
 
        self.datePicker = PickerKeyboard(dateFormatter: self.dateFormatter)
        
        
        self.user = User.mr_findFirstOrCreate(byAttribute: "userID", withValue: -1)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 57
        self.tableView.reloadData()
    }
    
    
    
    //MARK: - Api/Data
    private func register() {
        print(self.user)
        
        if !validate(notify: true) {
            
        }
        else {
            let a = UIAlertController(title: nil, message: "Sign Up Success", preferredStyle: .alert)
            a.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                self.dismiss(animated: true, completion: nil)
                self.cleanup()
            }))
            self.present(a, animated: true, completion: nil)
        }
    }
    
    
    
    private func validate(notify: Bool = false) -> Bool {
        guard let login = self.user.login?.characters.count, (3 ... 10).contains(login) else {
            if notify {
                UIAlertController.alert("Login should be 3...10 characters long", from: self)
            }
            return false
        }
        
        guard let password = self.user.password?.characters.count, (3 ... 10).contains(password) else {
            if notify {
                UIAlertController.alert("Password should be 3...10 characters long", from: self)
            }
            return false
        }
        
        guard let password2 = self.user.password2Transient, self.user.password! == password2 else {
            if notify {
                UIAlertController.alert("Passwords must be equal", from: self)
            }
            return false
        }
        
        guard let email = self.user.email, email.isValidEmail else {
            if notify {
                UIAlertController.alert("Email invalid", from: self)
            }
            return false
        }
        
        guard self.termsAgreement else {
            if notify {
                UIAlertController.alert("Please, agree with terms", from: self)
            }
            return false
        }
        
        
        //birtday, country isn't required for example

        
        return true
    }
    
    
    
    private func cleanup() {
        self.user.mr_deleteEntity()
    }
    
    
    
    
    //MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.cellsTypes.count
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let type = self.cellsTypes[indexPath.row]
            let reuseID: String = {
                var r = "\(SignUpCell.self)-"
                r.append(type == .birthday ? "RightIcon" : "LeftIcon")
                return r
            }()
            let c = tableView.dequeueReusableCell(withIdentifier: reuseID) as! SignUpCell
            c.type = type
            c.dateFormatter = self.dateFormatter
            c.data = self.user
            c.textField.delegate = self
            
            if c.type == .birthday {
                self.datePicker.assignedTextField = c.textField
            }
            
            return c
            
        case 1:
            let c = tableView.dequeueReusableCell(withIdentifier: "\(SignUpBottomCell.self)") as! SignUpBottomCell
            c.termsAgreement = self.termsAgreement
            c.signUpButton.replaceTarget(self, action: #selector(submitAction(_:)), for: .touchUpInside)
            c.signUpEnabled = self.validate(notify: false)
            c.delegate = self
            return c
            
        default:
            return UITableViewCell()
        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard let cell = textField.superview?.superview as? SignUpCell else {
            return true
        }
        
        switch cell.type {
        case .country:
            self.view.endEditing(true)
            self.showCountrySelect()
            return false
            
        default:
            return true
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let cell = textField.superview?.superview as? SignUpCell else {
            return true
        }
        
        let currentText = textField.text ?? ""
        guard let swiftRange = currentText.range(from: range) else {
            return false
        }
        let text = currentText.replacingCharacters(in: swiftRange, with: string)
        
        switch cell.type {
        case .login:
            self.user.login = text
            
        case .password:
            self.user.password = text
           
        case .password2:
            self.user.password2Transient = text
            
        case .email:
            self.user.email = text
            
        default:
            return false
        }
        
        self.formChanged()
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !self.tableView.focusNextTextField() {
            self.view.endEditing(true)
        }

        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let cell = textField.superview?.superview as? SignUpCell else {
            return
        }
        
        switch cell.type {
        case .birthday:
            self.user.birthday = self.dateFormatter.date(from: textField.text ?? "") as NSDate?
            
        default:
            break
        }
        
        self.formChanged()
    }
    

    
    
    
    
    
    //MARK: - UI Routines
    private func showCountrySelect() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "\(SelectCountryViewController.self)") as! SelectCountryViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    

    private func formChanged() {
        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .none)
    }
    
    
    
    //MARK: - Feedback
    func countrySelected(_ country: Country?) {
        guard let index = self.cellsTypes.index(of: .country) else {
            return
        }
        
        self.user.country = country
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        self.formChanged()
    }
   
    
    func termsAgreementChanged(agree: Bool) {
        self.termsAgreement = agree
        self.formChanged()
    }
    
    
    
    //MARK: - Actions
    @objc private func submitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.register()
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
        self.cleanup()
        self.dismiss(animated: true, completion: nil)
    }

    
}
