//
//  SelectCountryViewController.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/8/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit



protocol SelectCountryOutput {
    func countrySelected(_ country: Country?)
}


class SelectCountryViewController: UIViewController,
UITableViewDelegate,
UITableViewDataSource {
    

    
    
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Properties
    private var data: [Country] = []
    var delegate: SelectCountryOutput?
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        DataManager.shared.countries { (r) in
            self.data = r
            self.tableView.reloadData()
        }
    }
    
    
    
    
    //MARK - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "1")!
        c.textLabel?.text = self.data[indexPath.row].fullName
        return c
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.delegate?.countrySelected(self.data[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
}
