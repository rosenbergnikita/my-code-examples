//
//  SignUpBottomCell.swift
//  SignUp Screen
//
//  Created by Nikita Rosenberg on 4/9/17.
//  Copyright © 2017 RNA. All rights reserved.
//

import UIKit


protocol SignUpBottomCellOutput {
    func termsAgreementChanged(agree: Bool)
}


class SignUpBottomCell: UITableViewCell {
    
    
    //MARK: - Outlets
    @IBOutlet weak var checkbox: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    
    
    //MARK: - Properties
    var termsAgreement: Bool {
        set {
            self.checkbox.isSelected = newValue
        }
        get {
            return self.checkbox.isSelected
        }
    }
    var signUpEnabled: Bool! {
        didSet {
            self.signUpButton.isEnabled = signUpEnabled
        }
    }
    var delegate: SignUpBottomCellOutput?
    
    
    
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
    
    //MARK: - Action
    @IBAction func checboxAction(_ sender: Any) {
        self.termsAgreement = !self.termsAgreement
        self.delegate?.termsAgreementChanged(agree: self.termsAgreement)
    }
    
}
