//
//  DataController.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 6/5/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation
import CoreData




let DATA = DataController.sharedInstance


//typealias ResultBlock = (_ result: [NSManagedObject]) -> Void
typealias ResultBlock<T> = (_ result: [T]) -> Void


class DataController: NSObject  {
    
    // MARK: Lifecycle
    static let sharedInstance = DataController()
    private override init() {
        super.init()
    }
    

    //MARK: - Convenience
    //Object for user independent data
    lazy var persistent: Persistent = {
        let p = NSPredicate(format: "persistentID == \(INITIAL_ID)")
        var result:Persistent? = self.findObject(withPredicate: p)
        if result == nil {
            result = self.createObject() as Persistent
            result!.persistentID = Int32(INITIAL_ID)!
            //Default settings
            result!.settings = self.createObject() as Settings
            result!.settings?.updateCurrentLanguage()
        }
       
        return result!
    }()
    
    var currentCompany: Place? {
        get {
            return self.persistent.currentCompany
        }
        set {
            self.persistent.currentCompany = newValue
            Notify(.CURRENT_COMPANY_CHANGED)
        }
    }
    
    var currentUser: User? {
        get {
            return self.persistent.currentUser
        }
        set {
            if newValue?.serverID != self.currentUser?.serverID {
                self.deleteObject(self.currentUser)
            }
            self.persistent.currentUser = newValue
            Notify(.USER_CHANGED)
        }
    }
    
    
    
    
    
    // MARK: - Routines
    @discardableResult
    func findObjects(withPredicate p: NSPredicate?, withClassName className: String, sort: [NSSortDescriptor] = [], fetchOnlyID onlyID:Bool = false, _ asyncResultBlock: ResultBlock<NSManagedObject>? = nil) -> [NSManagedObject]? {
        
        let request = NSFetchRequest<NSManagedObject>()
        request.entity = NSEntityDescription.entity(forEntityName: className, in: self.managedObjectContext)
        request.sortDescriptors = sort
        request.predicate = p
        request.includesPropertyValues = !onlyID
        
        //Async
        if asyncResultBlock != nil {
            //Initialize Asynchronous Fetch Request
            let asyncRequest = NSAsynchronousFetchRequest(fetchRequest: request, completionBlock: { (asyncResult) in
                DispatchQueue.main.async {
                    let objects = asyncResult.finalResult ?? []
                    asyncResultBlock!(objects)
                }
            })
            
            //Execute Asynchronous Fetch Request
            self.managedObjectContext.perform {
                do {
                    _ = try self.managedObjectContext.execute(asyncRequest)
                }
                catch {
                    Log(self, level: .error, message: error.localizedDescription)
                    asyncResultBlock!([])
                }
            }
            
            return nil
        }
            
            
        //Sync
        else {
            var result: [NSManagedObject] = []
            do {
                result = try self.managedObjectContext.fetch(request)
            }
            catch {
                Log(self, level: .error, message: error.localizedDescription)
            }
            
            return result
        }
    }
    
    
    func findObjects<T:NSManagedObject>(withPredicate p: NSPredicate?, sort: [NSSortDescriptor] = []) -> [T] {
        return self.findObjects(withPredicate: p, withClassName: "\(T.self)", sort: sort) as? [T] ?? []
    }
    
    
    func findObject<T:NSManagedObject>(withPredicate p: NSPredicate?) -> T? {
        return self.findObject(withPredicate: p, withClassName: "\(T.self)") as? T
    }
    
    
    func findObject(withPredicate p: NSPredicate?, withClassName className: String) -> NSManagedObject? {
        if let objects = self.findObjects(withPredicate: p, withClassName: className), objects.count == 1 {
            return objects[0]
        }
        else {
            return nil
        }
    }
    
    
    func createObject<T:NSManagedObject>() -> T {
        let object = self.createObject(withClassName: "\(T.self)")
        return object as! T
    }
    
    
    func createObject(withClassName className: String) -> NSManagedObject {
        let object = NSEntityDescription.insertNewObject(forEntityName: className, into: self.managedObjectContext)
        //Touch to create apiObject. It's convinience because of relationship to apiObject is required
        if let o = object as? ApiObjectRuntime {
            _ = o.name
        }
        return object
    }
    
    
    func deleteAllObjects<T:NSManagedObject>(_ withClass: T.Type) {
        let allObjects = self.findObjects(withPredicate: nil, withClassName: "\(T.self)", sort: [], fetchOnlyID: true) as? [T] ?? []
        
        for object in allObjects {
            self.deleteObject(object)
        }
    }
    
    
    func deleteObjects(fromSet set: NSSet?) {
        guard let array = set?.allObjects as? [NSManagedObject] else {
            return
        }
        
        self.deleteObjects(fromArray: array)
    }
    
    
    func deleteObjects(fromArray array: [NSManagedObject]) {
        for object in array {
            self.deleteObject(object)
        }
    }
    
    
    func deleteObject(_ object: NSManagedObject?) {
        if object != nil {
            self.managedObjectContext.delete(object!)
        }
    }
    

    
    
    
    //MARK: - JSON Helpers
    func serverObject<T>(with objectID: String) -> T where T:NSManagedObject, T:ApiObjectProtocol {
        var result:T? = self.findObject(with: objectID)
        if result == nil {
            result = self.createObject() as T
            result!.serverID = objectID
        }
        
        return result!;
    }
    
    func createOrFindObject<T>(with JSON: [String:Any]) -> T where T:NSManagedObject, T:ApiObjectProtocol {
        guard let objectID = JSON[JK.ID.rawValue] as? String else {
            let result:T = self.createObject(with: JSON) as T
            return result
        }
        
        var result:T? = self.findObject(with: objectID)
        if result == nil {
            result = self.createObject(with: JSON) as T
            result!.serverID = objectID
        }
        
        return result!
    }
    
    
    func findObject<T>(with objectID: String) -> T? where T:NSManagedObject, T:ApiObjectProtocol {
        let p = NSPredicate(format: "\(ApiObjectPropertyServerID) == %@", objectID)
        let result:T? = self.findObject(withPredicate: p)
        return result
    }
    
    
    func createObject<T>(with JSON: [String:Any]) -> T where T:NSManagedObject, T:ApiObjectProtocol {
        let result = self.createObject() as T
        result.populate(with: JSON)
        return result
    }
    
    
    func unrecongnizedJSON(_ classType: AnyClass, JSON: Any, reason: String = "") {
        let message = "JSON unrecongnized for \(classType) (\(reason)): \(JSON)";
        Log(self, level: .warning, message: message)
    }
    

    
    //MARK: - Runtime helpers
    func array<T>(_ set: NSSet?, sortKey: String = Sort, asc: Bool = true) -> [T] where T:ApiObjectRuntime {
        var result = set?.sortedArray(using: [NSSortDescriptor(key: sortKey, ascending: asc)]) as? [T]
        
        result = result?.filter({ (o: T) -> Bool in
            return o.isDeleted == false
        })
        
        return result ?? []
    }
    
    
    
    //MARK: - Changes
    @discardableResult
    func changes(for field: FieldBase, withTypes: [ChangeType] = [.add, .set], onlyUnviewed: Bool = true, _ asyncResultBlock: ResultBlock<Change>? = nil) -> [Change]? {
        return self.changes(for: nil, field: field, withTypes: withTypes, onlyUnviewed: onlyUnviewed, asyncResultBlock)
    }
    
    
    @discardableResult
    func changes(for object: ObjectInCompany?, field: FieldBase? = nil, withTypes: [ChangeType] = [.add, .set], onlyUnviewed: Bool = true, _ asyncResultBlock: ResultBlock<Change>? = nil) -> [Change]? {
        guard let objectGuard = object ?? field?.objectInCompany else {
            asyncResultBlock?([])
            return nil
        }
        
        var predicates: [NSPredicate] = []
        predicates.append(NSPredicate(format: "(objectServerID == %@) AND (objectType == %@)", objectGuard.serverIDGuard, "\(type(of: objectGuard))"))
        
        if let fieldName = field?.systemName {
            predicates.append(NSPredicate(format: "(fieldName == %@)", fieldName))
        }
        
        if withTypes.count > 0 {
            var orPredicates: [NSPredicate] = []
            for t in withTypes {
                orPredicates.append(NSPredicate(format: "typeRaw == %@", t.rawValue))
            }
            predicates.append(NSCompoundPredicate(orPredicateWithSubpredicates: orPredicates))
        }
        
        if onlyUnviewed {
            predicates.append(NSPredicate(format: "viewed == NO"))
        }
        
        let p = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        if asyncResultBlock != nil {
            self.findObjects(withPredicate: p, withClassName: "\(Change.self)", sort: [], fetchOnlyID: true) { (result) in
                let changes = result as! [Change]
                asyncResultBlock!(changes)
            }
        }
        else {
            let changes = self.findObjects(withPredicate: p) as [Change]
            return changes
        }
        
        return nil
    }

    
    
    @discardableResult
    func changesCount(forArray array: [ApiObjectProtocol], _ resultBlock: @escaping ((Int) -> Void)) {
        var result = 0
        
        let count = array.count
        if count == 0 {
            resultBlock(result)
        }
        else {
            let group = DispatchGroup()
            for  item in array {
                group.enter()
                item.changesCount({ (r) in
                    result += r
                    group.leave()
                })
            }
            group.notify(queue: DispatchQueue.main, execute: { 
                resultBlock(result)
            })
        }
    }

    
    
    //MARK: - Feedback
    var feedbackTypesSorted: [FeedbackItemType] {
        return self.array(self.persistent.feedbackTypes, sortKey: ServerID, asc: true)
    }
    
    
    
    func populateFeedbackTypes(with feedbackTypesJSON: [String:[[String:Any]]]) {
        
        //sort, "0" must be first
        let feedbackTypesSorted = feedbackTypesJSON.sorted(by: { (dict1: (String, [[String : Any]]), dict2: (String, [[String : Any]])) -> Bool in
            return dict1.0.compare(dict2.0) == .orderedAscending
        })
        self.deleteObjects(fromSet: self.persistent.feedbackTypes)
        
        
        
        for (level, typesData) in feedbackTypesSorted {
            
            var parentType: FeedbackItemTypeLevel?
            if level != "0" {
                parentType = DATA.findObject(withPredicate: NSPredicate(format: "lvl == %@", level))
            }
            
            for typeData in typesData {
                if let lvl = typeData[JK.LVL.rawValue] as? String, !lvl.isEmpty {
                    let f = DATA.createObject(with: typeData) as FeedbackItemTypeLevel
                    f.persistentInverse = self.persistent
                }
                else {
                    let f = DATA.createObject(with: typeData) as FeedbackItemType
                    if parentType == nil {
                        f.persistentInverse = self.persistent
                    }
                    else {
                        f.parent = parentType
                    }
                }
            }
        }
    }

    
    
    
    //MARK: - Core Data stack
    let sqliteName = "db.sqlite"
    let coordinatorOptions = [NSMigratePersistentStoresAutomaticallyOption: true,
                                    NSInferMappingModelAutomaticallyOption: true]
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "df.s" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "model", withExtension: "momd")!
        if let mom = NSManagedObjectModel(contentsOf: modelURL) {
            return mom
        }
        else {
            Log(self, level: .error, message: "managedObjectModel loading failed, URL: \(modelURL), abort")
            abort()
        }
    }()
    
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {

        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent(self.sqliteName)
        Log(self, level: .info, message: "CoreData store:\n \(url.absoluteString)")
        
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                       configurationName: nil,
                                                       at: url,
                                                       options: self.coordinatorOptions)
        }
        catch {
            Log(self, level: .error, message: "Failed to initialize the application's saved data")
            self.tryRebuildCoreData(coordinator, url: url)
        }
        
        
        return coordinator
    }()
    
    
    func tryRebuildCoreData(_ coordinator: NSPersistentStoreCoordinator, url: URL) {
        
        let persistentStoreParentPath = self.applicationDocumentsDirectory.path
        let fileEnumerator = FileManager.default.enumerator(atPath: persistentStoreParentPath)
        while let path = fileEnumerator?.nextObject() as? String {
            if path.hasPrefix(self.sqliteName) || path.hasPrefix("."+self.sqliteName) {
                let pathToDelete = (persistentStoreParentPath as NSString).appendingPathComponent(path)
                do {
                    try FileManager.default.removeItem(atPath: pathToDelete)
                }
                catch {
                }
            }
        }
        
        
        for object in self.managedObjectContext.registeredObjects {
            self.managedObjectContext.delete(object)
        }
        
        
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                       configurationName: nil,
                                                       at: url,
                                                       options: self.coordinatorOptions)
        }
        catch {
            Log(self, level: .error, message: "Failed to rebuid core data, abort")
            abort()
        }
        
        Log(self, level: .warning, message: "CoreData dropped after failed migration")
    }
    
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    

    func save () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            }
            catch {
                Log(self, level: .error, message: "\(error)")
                abort()
            }
        }
    }    
    
}
