//
//  LocalizationController.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/6/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation
import Smartling_i18n
import Cache



let LOCALIZATION = LocalizationController.sharedInstance


func Localized(_ textCode: String) -> String {
    return LOCALIZATION.localized(textCode)
}

func LocalizedPlural(_ count: NSNumber, textCode: String) -> String {
    return LOCALIZATION.pluralTextLocalized(count, textCode: textCode)
}




class LocalizationController: NSObject {
    
    //MARK: - Lifecycle
    static let sharedInstance = LocalizationController()
    
    
    //MARK: - Properties
    var preferredLanguages: [String]  = {
        var result: [String] = []
        for language in Locale.preferredLanguages {
            let dict = Locale.components(fromIdentifier: language)
            if let languageCode = dict[NSLocale.Key.languageCode.rawValue]?.uppercased() {
                result.append(languageCode)
            }
        }
        
        var containsDefault = false
        for languageCode in result {
            if languageCode == APP_SETTINGS.defaultInterfaceLanguageCode {
                containsDefault = true
                break
            }
        }
        
        if !containsDefault {
            result.append(APP_SETTINGS.defaultInterfaceLanguageCode)
        }
        
        
        
        return result
    }()
    
    private lazy var settings: Settings = {
        return DATA.persistent.settings!
    }()
    
    
    
    //MARK: - Interface
    func localized(_ textCode: String) -> String {
        var result = ""
        for languageCode in self.preferredLanguages {
            result = self.localized(textCode, languageCode: languageCode)
            if result.isEmpty == false {
                break
            }
        }
        
        return result.isEmpty ? textCode : result
    }
    
    
    func localized(_ textCode: String, languageCode: String) -> String {
        if let result = self.localizedInCache(textCode, languageCode: languageCode) {
            return result
        }
        
        let p = NSPredicate(format: "apiObject.name == %@ AND languageCode == %@", textCode, languageCode.uppercased())
        let texts: [LocalizedText] = DATA.findObjects(withPredicate: p)
        guard let result = texts.first?.localized else {
            Log(self, level: .warning, message: "Failed to find localized string \(textCode) in language \(languageCode)")
            return ""
        }
        
        self.localizedInCache(textCode, languageCode: languageCode, set: result)
        
        return result
    }
    
    
    func pluralTextLocalized(_ number: NSNumber, textCode: String) -> String {
        let plural = self.pluralLocalized(number, textCode: textCode)
        return "\(number.stringValue) \(plural)"
    }
    
    
    func pluralLocalized(_ number: NSNumber, textCode: String) -> String {
        
        var result = ""
        for languageCode in self.preferredLanguages {
            
            //zero, one, two, few, many, other
            let pluralForm = self.pluralForm(number, languageCode: languageCode)
            var pluralSuffix = ""
            switch pluralForm {
            case "zero", "two": //not supported
                pluralSuffix = "other"
                
            default:
                pluralSuffix = pluralForm
                break
            }
            
            result = self.localized(textCode+"_"+pluralSuffix, languageCode: languageCode)
            
            if result.isEmpty == false {
                break
            }
        }
        
        return result.isEmpty ? textCode : result
    }
    
    
    
    private func pluralForm(_ number: NSNumber, languageCode: String) -> String {
        let languageCode_ns = (languageCode.lowercased() as NSString).utf8String
        let languageCode_cs = UnsafeMutablePointer<Int8>(mutating: languageCode_ns)
        let form_cs = pluralformf(languageCode_cs, number.floatValue)
        let form = String(cString: form_cs!)
        return form //?? "other"
    }
    
    
    
    
    //MARK: - Routines
    func updateLocalization() {

        //update 1 day
        let now = Date().timeIntervalSinceReferenceDate
        let lastUpdate = DATA.persistent.localizationLastUpdate
        if now-lastUpdate > 24*60*60 {
            self.settings.updateCurrentLanguage()
            DATA.deleteAllObjects(LocalizedText.self)
            self.loadLanguage()
        }
        else {
            Log(self, level: .info, message: "Localizadtion load skipped")
            self.finishLanguagesLoading(failure: false)
        }

    }
    
    
    private func loadLanguage(_ index: Int = 0) {

        guard let code = self.preferredLanguages[safe: index] , !code.isEmpty else {
            if index >= self.preferredLanguages.count {
                self.finishLanguagesLoading(failure: false)
            }
            else {
                Log(self, level: .error, message: "Failed to get localization code at index \(index) in languages:\n\(self.preferredLanguages)")
                self.loadLanguage(index+1)
            }
            return
        }
        
        
        API.localization(code,
                         success: {
                           self.loadLanguage(index+1)
            },
                         onFailure: { (response) in
                             self.finishLanguagesLoading(failure :true)
        })
    }
    
    
    
    
    private func finishLanguagesLoading(failure: Bool) {

        let message = failure ? "Failed to load localization" : ""
        let notificationStatus = failure ? NOTIFICATION_STATUS.failure : NOTIFICATION_STATUS.success
        let userInfo: [String:Any] = [NUIK.STATUS.rawValue: notificationStatus.rawValue,
                        NUIK.MESSAGE.rawValue: message]
        
        if failure {
            Log(self, level: .error, message: message)
        }
        else {
            DATA.persistent.localizationLastUpdate = Date().timeIntervalSinceReferenceDate
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: NK.LOCALIZATION_LOADED.rawValue), object: nil, userInfo:userInfo)
    }

    
    //MARK: - Cache
    private var cache = SyncCache(Cache<String>(name: "localization"))
    
    @discardableResult
    func localizedInCache(_ textCode: String, languageCode: String, set: String? = nil) -> String? {
        let key = "\(languageCode):\(textCode)"
        if set == nil {
            return cache.object(key)
        }
        else {
            cache.add(key, object: set!)
        }
        
        
        return nil
    }
}
