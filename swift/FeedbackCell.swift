//
//  FeedbackCell.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/16/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import UIKit


class FeedbackCell: HFTableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var container: UIView!

    
    //MARK: - Properties
    var data: FeedbackItem? {
        didSet {
            self.populateView()
        }
    }
    var feedbackView: FeedbackView?
    
    
    
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.keepSubviewBackground = true
    }
    
    /*
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.feedbackView?.layoutSubviews()
    }
 */
    
    
    override func populateView() {
        guard let dataGuard = self.data else {
            self.feedbackView?.removeFromSuperview()
            return
        }
        
        if self.feedbackView == nil {
            self.feedbackView = CreateView() as FeedbackView
            self.container.addSubview(self.feedbackView!)
            self.feedbackView!.translatesAutoresizingMaskIntoConstraints = false
            self.container.addConstraints([
                pin(self.container, toItem: self.feedbackView!, attribute: .topMargin),
                pin(self.container, toItem: self.feedbackView!, attribute: .bottomMargin),
                pin(self.container, toItem: self.feedbackView!, attribute: .leftMargin),
                pin(self.container, toItem: self.feedbackView!, attribute: .rightMargin),
                ])
        }
        
        self.feedbackView!.data = dataGuard
        
        if let userID = dataGuard.user?.serverID , userID == DATA.currentUser?.serverID {
            self.feedbackView!.menuButton.isHidden = false
            self.feedbackView!.menuButtonImage.isHidden = false
        }
        else {
            self.feedbackView!.menuButton.isHidden = true
            self.feedbackView!.menuButtonImage.isHidden = true
        }
        
        self.layoutIfNeeded()
    }
    
    
    func didEndDisplayingCell() {
        self.feedbackView?.didEndDisplayingCell()
    }

    
    
    
    //MARK: - Actions
   
    
}
