//
//  FeedbackView.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 8/2/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import UIKit



class FeedbackView: UIView,
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    //MARK: - Outlets
    @IBOutlet weak var headerSection: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: HFLabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var menuButtonImage: UIImageView!
    @IBOutlet weak var commentDate: HFLabel!
    
    @IBOutlet weak var contentSection: UIView!
    @IBOutlet weak var message: HFLabel!
    @IBOutlet weak var detailButton: HFButton!
    @IBOutlet weak var detailButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var translateButton: HFButton!
    @IBOutlet weak var photosSubsection: UIView!
    @IBOutlet weak var photosSubsectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    
    @IBOutlet weak var commentSection: UIView!
    @IBOutlet weak var commentWrapper: UIView!
    @IBOutlet weak var commentSectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var footerSection: UIView!
    @IBOutlet weak var footerSectionHeight: NSLayoutConstraint!
    @IBOutlet weak var comments: UIButton!
    @IBOutlet weak var likes: UIButton!
    @IBOutlet weak var type: HFLabel!
    @IBOutlet weak var problemStatus: UILabel!
    
    
    
    
    //MARK: - Properties
    var data: FeedbackItem? {
        didSet {
            self.populateView()
        }
    }
    static let defaultUserImage = UIImage(named: "opinions_avatar")
    private var showFullText = false
    
    private var feedbackView: FeedbackView?
    private var photosData: [HFPhoto] = []
    private var photosCollectionSpacing = 2.0
    private var photosViewRatioHeights: [Double] = []
    private var photosViewRatioWidths: [[Double]] = []
    

    
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.photosCollectionView.delegate = self
        self.photosCollectionView.dataSource = self
        self.photosCollectionView.register(UINib(nibName: "\(PhotoCell.self)", bundle: nil), forCellWithReuseIdentifier: "cell")
        (self.photosCollectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing = CGFloat(self.photosCollectionSpacing)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.userImage.layer.cornerRadius = self.userImage.frame.size.width/2.0
        self.type.layer.cornerRadius = 3
        
        var hideDetail = true
        var showLines = 0
        if !self.showFullText {
            //check necessity to show detail label
            let messageText = self.message.text ?? ""
            let height = self.message.font.heightOfString(messageText, constrainedToWidth: self.message.frame.size.width)
            let lines = Int(ceil(height)/self.message.font.lineHeight)
            if lines > UI.settings.maxFeedbackLines {
                showLines = UI.settings.maxFeedbackLines
                hideDetail = false
            }
        }
        
        self.message.numberOfLines = showLines
        self.hide(detailButton: hideDetail, translateButton: true)
        


        self.photosCollectionView.reloadData()
        self.photosCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    func didEndDisplayingCell() {
        self.feedbackView?.didEndDisplayingCell()
        //SDWebImage loading, cell reusing...
        self.userImage.image = FeedbackView.defaultUserImage
    }
    

    
    
    
    //MARK: - Lifecycle (Populate)
    private func populateView() {
        guard let dataGuard = self.data else {
            return
        }
        
        
        self.userName.text = dataGuard.user?.fullName
        if let url = URLFromString(dataGuard.user?.imageURL) {
            self.userImage.sd_setImage(with: url)
        }
        
        
        let dateAdd = Date.init(timeIntervalSinceReferenceDate: dataGuard.date)
        self.commentDate.text = DATA.currentCompany?.stringFromDate(dateAdd, format: "dd MMMM y HH:mm")
        
        
        self.populateAdminComment(self.data?.adminComment)
        self.populateViewComments(dataGuard)
        self.populateViewLikes()
        self.populatePhotos(dataGuard)
        
        self.showFullText = false
        self.message.attributedText = dataGuard.messageFull
        
        var opinionTypeColor = UIColor.clear
        var opinionTypeText: String = ""
        switch dataGuard.typeEnum {
        case .idea:
            opinionTypeColor = Color(r: 253, g: 216, b: 53)
            opinionTypeText = Localized("review_type_idea")
            
        case .question:
            opinionTypeColor = Color(r: 66, g: 133, b: 244)
            opinionTypeText = Localized("review_type_question")

        case .complain:
            opinionTypeColor = Color(r: 219, g: 68, b: 55)
            opinionTypeText = Localized("review_type_abuse")
            
        case .review:
            opinionTypeColor = Color(r: 76, g: 175, b: 80)
            opinionTypeText = Localized("review_type_сomment")
            break

        case .problem_tech, .problem_health, .problem_lost:
            opinionTypeColor = UI.settings.purpleColor
            opinionTypeText = Localized("review_type_problem")
            
        default:
            break
        }
        self.type.backgroundColor = opinionTypeColor
        self.type.text = "   \(opinionTypeText)   "
        self.problemStatus.text = dataGuard.problemStatus
    }
    
    
    
    private func populateAdminComment(_ data: FeedbackItem?) {
        guard let dataGuard = data else {
            self.feedbackView?.removeFromSuperview()
            self.hideAdminComment(true)
            return
        }
        
        self.hideAdminComment(false)
        if self.feedbackView == nil {
            self.feedbackView = CreateView() as FeedbackView
            self.feedbackView!.hideFooter()
            self.commentWrapper.addSubview(self.feedbackView!)
            self.feedbackView!.translatesAutoresizingMaskIntoConstraints = false
            self.commentWrapper.addConstraints([
                pin(self.commentWrapper, toItem: self.feedbackView!, attribute: .topMargin),
                pin(self.commentWrapper, toItem: self.feedbackView!, attribute: .bottomMargin),
                pin(self.commentWrapper, toItem: self.feedbackView!, attribute: .leftMargin),
                pin(self.commentWrapper, toItem: self.feedbackView!, attribute: .rightMargin),
                ])
        }
        
        self.feedbackView!.data = dataGuard
        self.feedbackView!.menuButton.isHidden = true
        self.feedbackView!.menuButtonImage.isHidden = true
    }
    
    
    private func populateViewComments(_ d: FeedbackItem) {
        let commentsCount = d.comments?.count ?? 0
        self.comments.setTitle("\(commentsCount)", for: UIControlState())
        self.comments.tintColor = commentsCount > 0 ? UI.settings.orangeColor : UI.settings.greenColor
        
    }
    
    
    func populateViewLikes() {
        var likesCount = self.data?.likesCount ?? 0
        likesCount = likesCount > 0 ? likesCount : 0
        self.likes.setTitle("\(likesCount)", for: UIControlState())
        self.likes.tintColor = likesCount > 0 ? UI.settings.orangeColor : UI.settings.greenColor
    }
    
    
    private func populatePhotos(_ d: FeedbackItem) {
        self.photosData = d.photosSorted
        let count = self.photosData.count
        if count == 0 {
            self.hidePhotos(true)
            return
        }
        self.hidePhotos(false)
        
        let countFloat = Float(count)
        let root = sqrt(countFloat)
        let rootFraction = Int(modf(root).0)
        var rows = rootFraction + 1
        let perRow = rootFraction
        let lastElements = count - rootFraction*rootFraction
        if lastElements == 0 {
            rows -= 1
        }

        
        self.photosViewRatioHeights = []
        self.photosViewRatioWidths = []
        
        let calcElementsInRow = { (lastRow: Bool, rows: Int, lastElements: Int) -> Int in
            if lastRow && rows == 1 {
                return perRow
            }
            else if lastRow && rows > 1 {
                return lastElements
            }
            else {
                return perRow
            }
        }
        
        let midRatioForRow = 1.0 / Double(rows)
        var totalHeightRatio = 0.0
        
        
        
        for row in (0 ..< rows) {
            let lastRow = row == rows-1
            var heightRatio = 0.0
            if lastRow {
                heightRatio = 1.0 - totalHeightRatio
            }
            else {
                heightRatio = (midRatioForRow*0.8 ... midRatioForRow*1.2).random()
            }
            totalHeightRatio += heightRatio
            self.photosViewRatioHeights.append(heightRatio)
            
            let elementsInRow = calcElementsInRow(lastRow, rows, lastElements)
            //1.0 is 100% of width
            let midRatioForElement = 1.0 / Double(elementsInRow)
            var totalElementsRatio = 0.0
            var result: [Double] = []
            for element in (0 ..< elementsInRow) {
                let lastElement = element == elementsInRow-1
                var elementRatio = 0.0
                if lastElement {
                    elementRatio = 1.0 - totalElementsRatio
                }
                else {
                    elementRatio = (midRatioForElement*0.8 ... midRatioForElement*1.2).random()
                }
                totalElementsRatio += elementRatio
                result.append(elementRatio)
            }
            self.photosViewRatioWidths.append(result)
        }

    }
    
    

    
    
    //MARK: - UI Routines
    private func hideAdminComment(_ hide: Bool = true) {
        self.commentSection.isHidden = hide
        self.commentSectionHeight.priority = hide ? 999 : 1
        self.commentSectionHeight.constant = hide ? 0 : 40
    }
    
    
    private func hideFooter(_ hide: Bool = true) {
        self.footerSection.isHidden = hide
        self.footerSectionHeight.constant = hide ? 0 : 40        
    }
    
    
    private func hidePhotos(_ hide: Bool = true) {
        self.photosSubsection.isHidden = hide
        self.photosSubsectionHeight.constant = hide ? 0 : 350
    }
    
    
    private func hide(detailButton: Bool, translateButton: Bool) {
        self.detailButton.isHidden = detailButton
        self.translateButton.isHidden = translateButton
        if detailButton && translateButton {
            self.detailButtonHeight.constant = 0
        }
        else {
            self.detailButtonHeight.constant = 25
        }
    }
    
    
    
    
    //MARK: - Collection View (Photos)
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.photosViewRatioHeights.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photosViewRatioWidths[safe: section]?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuse = "cell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuse, for: indexPath) as! PhotoCell
        
        cell.photo = self.photosData[safe: self.linearIndex(for: indexPath)]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = self.photosData[safe: self.linearIndex(for: indexPath)]
        let vc = PhotosViewer(photos: self.photosData, initialPhoto: photo)
        UI.showModal(vc)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let countRows = self.photosViewRatioHeights.count
        let countColumns = self.photosViewRatioWidths[safe: indexPath.section]?.count ?? 0
        let heightRatio = CGFloat(self.photosViewRatioHeights[safe: indexPath.section] ?? 0.0)
        let widthRatio = CGFloat(self.photosViewRatioWidths[safe: indexPath.section]?[safe: indexPath.row] ?? 0.0)
        let defaultHeight = collectionView.frame.size.height - CGFloat(self.photosCollectionSpacing) * CGFloat(countRows-1)
        let defaultWidth = collectionView.frame.size.width - CGFloat(self.photosCollectionSpacing) * CGFloat(countColumns-1)
        let height = (defaultHeight * heightRatio) - 0.3 //insure to size won't exceed borders
        let width = (defaultWidth * widthRatio) - 0.3
        
        let result = CGSize(width: width > 0 ? width : 0 , height: height > 0 ? height : 0)
        return result
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: CGFloat(self.photosCollectionSpacing), right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(self.photosCollectionSpacing)
    }
    
    


    func linearIndex(for indexPath: IndexPath) -> Int {
        var result = 0
        for s in (0 ..< indexPath.section) {
            let perRow = self.photosViewRatioWidths[safe: s]?.count ?? 0
            result += perRow
        }
        result += indexPath.row
        return result
    }
    

    
    
    
    //MARK: - Actions
    @IBAction func detailAction(_ sender: Any) {
        self.showFullText = true
        self.message.numberOfLines = 0
        self.detailButton.isHidden = true
        //self.layoutIfNeeded()
        
        Notify(NK.FEEDBACK_DETAIL_ACTION)
    }
    
    
    @IBAction func menuAction(_ sender: Any) {
        Notify(NK.SHOW_FEEDBACKITEM_MENU, object:self.data)
    }
    
    
    @IBAction func translateAction(_ sender: Any) {
        
    }
    
    
    @IBAction func commentsAction(_ sender: Any) {
        Notify(NK.SHOW_FEEDBACKITEM_COMMENTS, object:self.data)
    }
    
    
    @IBAction func likeAction(_ sender: Any) {
        Notify(NK.LIKE_FEEDBACKITEM, object:self.data)
    }
    
    
}
