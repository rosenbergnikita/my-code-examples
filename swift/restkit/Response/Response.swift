//
//  Error.swift
//  -
//
//  Created by Nikita Rosenberg on 3/7/17.
//  Copyright © 2017 -. All rights reserved.
//

import Foundation



@objc
class Response: NSObject {
    var message: String?
    var raw: Any?
}





@objc
class ResponseError: Response, Error {
    
    var apiErrorCode: NSNumber!
    var apiErrorMessage: String!
    
    
    var localizedDescription: String {
        return self.apiErrorMessage ?? ""
    }
    
    convenience override init() {
        self.init(apiErrorCode: nil, apiErrorMessage: nil)
    }
    
    convenience init(otherError: Error?) {
        self.init(apiErrorCode: nil, apiErrorMessage: otherError?.localizedDescription)
    }

    
    init(apiErrorCode: NSNumber?, apiErrorMessage: String?) {
        self.apiErrorCode = apiErrorCode ?? 0
        self.apiErrorMessage = apiErrorMessage ?? ""
        super.init()
    }
    
    
}
