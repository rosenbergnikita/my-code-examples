//
//  Mappings.swift
//  -
//
//  Created by Nikita Rosenberg on 3/2/17.
//  Copyright © 2017 -. All rights reserved.
//

import Foundation


extension Request {
    
    
    static var baseResponsePath = "response.data" //used for error mapping too
    var responsePath: String {
        let base = type(of: self).baseResponsePath
        
        switch self {
        case .reports,
             .report,
             .week,
             .avatars,
             .seedbanks,
             .comments:
            return base + "." + self.rawValue
            
        case .login,
             .registrationEmail,
             .registrationSocial,
             .grower:
            return base + ".user"
            
        case .recoveryPassword,
             .setMeasure,
             .removeComment:
            return base + ".result"
            
        case .nutrients:
            return base + ".bank_nutrients"
            
        case .saveReport:
            return base + ".report"
            
        case .saveWeek,
             .saveHarvest,
             .likeWeek:
            return base + ".week"
            
        case .likeComment,
             .saveComment:
            return base + ".comment"
            
            
        case .checkSession:
            return base + ".session_validate"
            
        case .logout:
            return base + ".session_logout"
            
        default:
            return base
        }
    }
    
    
    
    
    func mapping(forStore store: RKManagedObjectStore) -> RKMapping? {
        switch self {
            
        case .reports,
             .report,
             .saveReport:
            return Report.self.mapping(store)
        
        case .login,
             .registrationEmail,
             .registrationSocial,
             .grower:
            return User.self.mapping(store)
        
        case .recoveryPassword,
             .setMeasure,
             .removeComment,
             .checkSession,
             .logout:
            return Response.self.mapping()
            
        case .avatars:
            return RandomAvatar.self.mapping()
        
        case .week,
             .saveWeek,
             .saveHarvest,
             .likeWeek:
            return Week.self.mapping(store)
          
        case .seedbanks:
            return Seedbank.self.mapping(store)
            
        case .nutrients:
            return NutrientBank.self.mapping(store)
            
        case .comments,
             .likeComment,
             .saveComment:
            return Comment.self.mapping(store)
            
        default:
            return nil
        }
    }

    
    
    static var commonErrorMapping: RKObjectMapping? {
        let m = RKObjectMapping(for: ResponseError.self)
        m?.addAttributeMappings(from: ["error": "apiErrorMessage",
                                       "error_code": "apiErrorCode"])

        return m
    }
    
    
    

    
    
    
    var route: RKRoute? {
        return nil
    }
}




//MARK: - Entity common mappings
protocol HaveMapping {
    static func mapping(_ store: RKManagedObjectStore?) -> RKMapping
}



extension Report: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        m.identificationAttributes = ["reportID"]
        m.addAttributeMappings(from: ["id": "reportID",
                                       "name": "name",
                                       "grow_score": "growScore",
                                       "last_update": "lastUpdate",
                                       "cover": "cover",
                                       "stage": "stage",
                                       "stage_num": "stageNumber",
                                       "current_week": "currentWeek",
                                       "cnt_update": "countUpdate",
                                       "cnt_views": "countViews",
                                       "cnt_likes": "countLikes",
                                       "cnt_comments": "countComments",
                                       "enable_harvest": "enableHarvest",
                                       "params.lights_flo.id": "transientLightFloID",
                                       "params.lights_veg.id": "transientLightVegID",
                                       "params.room": "transientRoomID",
                                       "params.soil": "transientSoilID",
                                       "params.method": "transientMethodIDs",
                                       "last_week_id": "lastWeekID"])
        
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: nil, toKeyPath: "author", with: User.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "strains", toKeyPath: "strains", with: Strain.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "weeks", toKeyPath: "weeks", with: Week.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "bank_nutrients", toKeyPath: "usedNutrientBanks", with: UsedNutrientBank.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: nil, toKeyPath: "comments", with: Comment.self.mapping(store)))
        
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "params.lights_flo.watts", toKeyPath: "lightFloWatts", with: SimpleValue.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "params.lights_veg.watts", toKeyPath: "lightVegWatts", with: SimpleValue.self.mapping(store)))

        
        m.addConnection(forRelationship: "lightFlo", connectedBy: ["transientLightFloID": "itemID"])
        m.addConnection(forRelationship: "lightVeg", connectedBy: ["transientLightVegID": "itemID"])
        m.addConnection(forRelationship: "room", connectedBy: ["transientRoomID": "itemID"])
        m.addConnection(forRelationship: "soil", connectedBy: ["transientSoilID": "itemID"])
        m.addConnection(forRelationship: "methods", connectedBy: ["transientMethodIDs": "itemID"])
        
        return m
    }
}



extension Strain: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        m.identificationAttributes = ["strainID"]
        m.addAttributeMappings(from: ["id": "strainID",
                                      "enable_harvest": "transientHarvestID",
                                      "custom_name": "customName"])
        
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: nil, toKeyPath: "seedbank", with: Seedbank.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: nil, toKeyPath: "seed", with: Seed.self.mapping(store)))
        m.addConnection(forRelationship: "harvest", connectedBy: ["transientHarvestID": "harvestID"])
        
        return m
    }
}



extension Seed: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        //reports
        let query1 = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        query1.identificationAttributes = ["seedID"]
        query1.addAttributeMappings(from: ["seed_id": "seedID",
                                           "seed_name": "name",
                                           "seed_section": "section"])

        
        //get_breeders
        let query2 = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        query2.identificationAttributes = ["seedID"]
        query2.addAttributeMappings(from: ["id": "seedID",
                                           "name": "name",
                                           "section": "section",
                                           "cnt_reports": "countReports",
                                           "cnt_growers": "countGrowers",
                                           "photo" : "photo",
                                           "photo_small": "photoSmall",
                                           "photo_medium": "photoMedium"])
        
        let m = RKDynamicMapping()
        m.addMatcher(RKObjectMappingMatcher(keyPath: "seed_id", expectedClass: NSString.self, objectMapping: query1))
        m.addMatcher(RKObjectMappingMatcher(keyPath: "id", expectedClass: NSString.self, objectMapping: query2))
        return m
    }
}



extension Seedbank: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        //reports
        let query1 = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        query1.identificationAttributes = ["bankID"]
        query1.addAttributeMappings(from: ["bank_id": "bankID",
                                           "bank_name": "name",
                                           "logo": "logo",
                                           "bank_section": "section"])
        ///get_breeders
        let query2 = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        query2.identificationAttributes = ["bankID"]
        query2.addAttributeMappings(from: ["id": "bankID",
                                           "name": "name",
                                           "logo": "logo",
                                           "section": "section",
                                           "cnt_reports": "countReports",
                                           "cnt_growers": "countGrowers"])
        query2.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "seeds", toKeyPath: "seeds", with: Seed.self.mapping(store)))
        
        let m = RKDynamicMapping()
        m.addMatcher(RKObjectMappingMatcher(keyPath: "bank_id", expectedClass: NSString.self, objectMapping: query1))
        m.addMatcher(RKObjectMappingMatcher(keyPath: "id", expectedClass: NSString.self, objectMapping: query2))
        return m
    }
}



extension User: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let query1 = RKEntityMapping(forEntityForName: "\(self)", in: store!)!
        query1.identificationAttributes = ["userID"]
        query1.addAttributeMappings(from: ["id": "userID",
                                           "name": "name",
                                           "avatar": "avatar",
                                           "avatar_small": "avatarSmall",
                                           "status": "status",
                                           "user_session" : "sessionToken",
                                           "length": "lengthRaw",
                                           "weight": "weightRaw",
                                           "temperature": "temperatureRaw",
                                           "nutrient": "nutrientRaw",
                                           "date_registration": "registrationDate"])
        
        let query2 = RKEntityMapping(forEntityForName: "\(User.self)", in: store)!
        query2.identificationAttributes = ["userID"]
        query2.addAttributeMappings(from: ["user_id": "userID",
                                           "user_name": "name",
                                           "user_avatar": "avatar",
                                           "user_status": "status"])
        
        //comments
        let query3 = RKEntityMapping(forEntityForName: "\(User.self)", in: store)!
        query3.identificationAttributes = ["userID"]
        query3.addAttributeMappings(from: ["user_id": "userID",
                                           "username": "name",
                                           "avatar": "avatar",
                                           "status": "status"])

 
        
        let m = RKDynamicMapping()
        m.addMatcher(RKObjectMappingMatcher(keyPath: "username", expectedClass: NSString.self, objectMapping: query3))
        m.addMatcher(RKObjectMappingMatcher(keyPath: "user_id", expectedClass: NSNumber.self, objectMapping: query2))
        m.addMatcher(RKObjectMappingMatcher(keyPath: "id", expectedClass: NSNumber.self, objectMapping: query1))
        return m
    }
}



extension Week: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let query1 = RKEntityMapping(forEntityForName: "\(self)", in: store!)!
        query1.identificationAttributes = ["weekID"]
        query1.addAttributeMappings(from: ["week_id": "weekID",
                                      "week_number": "number",
                                      "week_number_name": "numberName",
                                      "week_stage": "stageNumber",
                                      "week_comment": "countComments",
                                      "week_like": "countLikes",
                                      "week_photo": "countPhotos",
                                      "week_updated": "gdUpdated",
                                      "user_like": "myLike", //FIXME: make relations to users liked
                                      "week_stage_name": "stage",
                                      "height": "height",
                                      "air_temp": "airTemp",
                                      "ph": "ph",
                                      "ppm": "ppm",
                                      "air_hum": "airHum",
                                      "light": "light",
                                      "create_at": "created",
                                      "comment": "comment",
                                      "id_user": "transientUserID",
                                      "report_id": "transientReportID"])

        query1.addConnection(forRelationship: "report", connectedBy: ["transientReportID": "reportID"])
        query1.addConnection(forRelationship: "author", connectedBy: ["transientUserID": "userID"])
        
        query1.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "photos", toKeyPath: "photos", with: Photo.self.mapping(store)))
        query1.addPropertyMapping(RKRelationshipMapping(fromKeyPath: nil, toKeyPath: "harvest", with: Harvest.self.mapping(store)))
        query1.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "bank_nutrients", toKeyPath: "usedNutrientBanks", with: UsedNutrientBank.self.mapping(store)))
        query1.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "comments", toKeyPath: "comments", with: Comment.self.mapping(store)))
        

        let query2 = RKEntityMapping(forEntityForName: "\(self)", in: store!)!
        query2.identificationAttributes = ["weekID"]
        query2.addAttributeMappings(from: ["@metadata.query.parameters.\(RequestParameters.Key.week_id.rawValue)": "weekID",
                                           "week_like": "countLikes"])
        
        
        let m = RKDynamicMapping()
        m.addMatcher(RKObjectMappingMatcher(keyPath: "week_id", expectedClass: NSNumber.self, objectMapping: query1))
        m.addMatcher(RKObjectMappingMatcher(keyPath: "week_id", expectedClass: NSString.self, objectMapping: query1))
        m.addMatcher(RKObjectMappingMatcher(keyPath: "week_like", expectedClass: NSString.self, objectMapping: query2))
        return m
    }
}



extension Harvest: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store!)!
        m.identificationAttributes = ["harvestID"]
        m.addAttributeMappings(from: ["id_harvest": "harvestID",
                                      "comment_harvest": "comment",
                                      "n_weight": "weight",
                                      "n_plant": "plantsCount",
                                      "n_watt": "watt",
                                      "n_space": "space",
                                      "strain_general": "strainGeneral",
                                      "strain_comment": "strainComment",
                                      "strain_growing": "strainGrowing",
                                      "strain_resistance": "strainResistance",
                                      "strain_indica": "strainIndica"])
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "strain", toKeyPath: "strain", with: Strain.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "taste", toKeyPath: "tastes", with: Taste.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "positive_effect", toKeyPath: "positiveEffects", with: PositiveEffect.self.mapping(store)))
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "negative_effect", toKeyPath: "negativeEffects", with: NegativeEffect.self.mapping(store)))
        
        return m
    }
}




extension Photo: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store!)!
        m.identificationAttributes = ["photoID"]
        m.addAttributeMappings(from: ["id": "photoID",
                                      "comment": "comment",
                                      "photo_s": "s",
                                      "photo_m": "m",
                                      "photo_xs": "xs",
                                      "video" : "video"])
        return m
    }
}



extension RandomAvatar: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKObjectMapping(for: RandomAvatar.self)!
        m.addPropertyMapping(RKAttributeMapping(fromKeyPath: nil, toKeyPath: "url"))
        return m
    }
}



extension Response: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKObjectMapping(for: Response.self)!
        m.addPropertyMapping(RKAttributeMapping(fromKeyPath: nil, toKeyPath: "message"))
        m.addPropertyMapping(RKAttributeMapping(fromKeyPath: nil, toKeyPath: "raw"))
        return m
    }
}



extension DigestItem: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store!)!
        m.identificationAttributes = ["itemID"]
        m.addAttributeMappings(from: ["id": "itemID",
                                      "name": "name",
                                      "name_full": "fullName",
                                      "n_order": "order"])
        return m
    }
}


extension SimpleValue: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store!)!
        m.addPropertyMapping(RKAttributeMapping(fromKeyPath: nil, toKeyPath: "value"))
        return m
    }
}


extension GD_Color {
    override class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let base = super.mapping(store)
        guard let m = base as? RKEntityMapping else {
            return base
        }
        m.addAttributeMappings(from: ["color": "hex"])
        return m
    }
}


extension Effect {
    override class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let base = super.mapping(store)
        guard let m = base as? RKEntityMapping else {
            return base
        }
        m.addAttributeMappings(from: ["ico": "ico"])
        return m
    }
}


extension Taste {
    override class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let base = super.mapping(store)
        guard let m = base as? RKEntityMapping else {
            return base
        }
        m.addAttributeMappings(from: ["ico": "ico"])
        return m
    }
}



extension Nutrient: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        m.identificationAttributes = ["nutrientID"]
        m.addAttributeMappings(from: ["id": "nutrientID",
                                      "name": "name",
                                      "section": "section",
                                      "photo" : "photo",
                                      "photo_small" : "photoSmall",
                                      "cnt_reports": "countReports",
                                      "cnt_growers": "countGrowers"])
        return m
    }
}


extension NutrientBank: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        m.identificationAttributes = ["bankID"]
        m.addAttributeMappings(from: ["id": "bankID",
                                      "name": "name",
                                      "section": "section",
                                      "logo" : "logo",
                                      "cnt_reports": "countReports",
                                      "cnt_growers": "countGrowers"])
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "nutrients", toKeyPath: "nutrients", with: Nutrient.self.mapping(store)))
        return m
    }
}


extension UsedNutrientBank: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        m.addAttributeMappings(from: ["id": "transientNutrientBankID"])
        m.addConnection(forRelationship: "nutrientBank", connectedBy: ["transientNutrientBankID": "bankID"])
        
        m.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "nutrients", toKeyPath: "usedNutrients", with: UsedNutrient.self.mapping(store)))
        
        return m
    }
}



extension UsedNutrient: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        m.addAttributeMappings(from: ["color": "color",
                                      "value": "value",
                                      "custom_name": "nameCustom",
                                      "id": "transientNutrientID"])
        m.addConnection(forRelationship: "nutrient", connectedBy: ["transientNutrientID": "nutrientID"])
        return m
    }
}





extension Comment: HaveMapping {
    class func mapping(_ store: RKManagedObjectStore? = nil) -> RKMapping {
        let m1 = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        m1.identificationAttributes = ["commentID"]
        m1.addAttributeMappings(from: ["id": "commentID",
                                      "comment": "comment",
                                      "cnt_replies": "countRepliesCommon",
                                      "cnt_reply": "countReplies",
                                      "cnt_likes": "countLikes",
                                      "is_liked": "myLike",
                                      "is_reply": "isReply",//FIXME: make relations to users liked
                                    "@metadata.query.parameters.\(RequestParameters.Key.report_id.rawValue)": "transientReportID",
                                    "@metadata.query.parameters.\(RequestParameters.Key.week_id.rawValue)": "transientWeekID",
                                    "@metadata.query.parameters.\(RequestParameters.Key.comment_id_reply.rawValue)": "transientCommentID"
                                      ])
        
        m1.addPropertyMapping(RKRelationshipMapping(fromKeyPath: nil, toKeyPath: "author", with: User.self.mapping(store)))
        m1.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "replies", toKeyPath: "replies", with: m1))
        
        m1.addConnection(forRelationship: "report", connectedBy: ["transientReportID": "reportID"])
        m1.addConnection(forRelationship: "week", connectedBy: ["transientWeekID": "weekID"])
        m1.addConnection(forRelationship: "replyForComment", connectedBy: ["transientCommentID": "commentID"])
        
        
        let user = RKEntityMapping(forEntityForName: "\(User.self)", in: store)!
        user.identificationAttributes = ["userID"]
        user.addAttributeMappings(from: ["id_for": "userID",
                                         "name_for": "name",
                                         "avatar_for": "avatar"])
        m1.addPropertyMapping(RKRelationshipMapping(fromKeyPath: nil, toKeyPath: "replyForUser", with: user))
        
        
        
        let m2 = RKEntityMapping(forEntityForName: "\(self)", in: store)!
        m2.identificationAttributes = ["commentID"]
        m2.addAttributeMappings(from: ["@metadata.query.parameters.\(RequestParameters.Key.comment_id.rawValue)": "commentID"])

        
        
        let m = RKDynamicMapping()
        m.addMatcher(RKObjectMappingMatcher(keyPath: "id", expectedClass: NSString.self, objectMapping: m1))
        m.addMatcher(RKObjectMappingMatcher(keyPath: "cnt_likes", expectedClass: NSString.self, objectMapping: m2))
        return m
    }
}
