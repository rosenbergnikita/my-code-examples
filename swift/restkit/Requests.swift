//
//  Requests.swift
//  -
//
//  Created by Nikita Rosenberg on 3/2/17.
//  Copyright © 2017 -. All rights reserved.
//

import Foundation



enum Request: String, EnumCollection {
    case reports
    case report
    case saveReport
    case saveWeek
    case saveHarvest
    case week
    case comments
    case likeWeek
    case likeComment
    case removeComment
    case saveComment
    
    
    case login
    case registrationEmail
    case registrationSocial
    case recoveryPassword
    case avatars
    case setMeasure
    case checkSession
    case grower
    case logout
    
    case digest
    case seedbanks
    case nutrients
    
    

    
    
    var pathPattern: String {
        switch self {
        case .report:
            return "/reports/detail/:\(RequestParameters.Key.reportID.rawValue)"
            
        case .week:
            return "/reports/:\(RequestParameters.Key.reportID.rawValue)/week/:\(RequestParameters.Key.weekID.rawValue)"

        case .login:
            return "/growers/login"
            
        case .registrationEmail:
            return "/growers/email_register"
        
        case .registrationSocial:
            return "/growers/social_register"
            
        case .recoveryPassword:
            return "/growers/forgot_password_session"
            
        case .avatars:
            return "/digest/get_avatars"
            
        case .seedbanks:
            return "/digest/get_breeders"
            
        case .nutrients:
            return "/digest/get_nutrients"
            
        case .setMeasure:
            return "/grower/set_measure"
        
        case .comments:
            return "/reports/comments"
            
        case .saveReport:
            return "/reports/save_report"
            
        case .saveWeek:
            return "/reports/save_week"
            
        case .saveHarvest:
            return "/reports/save_harvest"
            
        case .likeWeek:
            return "/reports/like_week"
            
        case .likeComment:
            return "/reports/like_comment"
            
        case .removeComment:
            return "/reports/remove_comment"
            
        case .saveComment:
            return "/reports/save_comment"
            
        case .checkSession:
            return "/growers/check"
            
        case .grower:
            return "/growers/profile/:\(RequestParameters.Key.name.rawValue)"
            
        case .logout:
            return "/growers/logout"
            
        default:
            return "/" + self.rawValue
        }
    }
    
    
    
    var query: RequestParameters {
        return RequestParameters(request: self)
    }
    
    
    func query(_ params: [RequestParameters.Key: Any]) -> RequestParameters {
        return RequestParameters(request: self, params)
    }
}
