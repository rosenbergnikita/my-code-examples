//
//  RequestParameters.swift
//  -
//
//  Created by Nikita Rosenberg on 3/2/17.
//  Copyright © 2017 -. All rights reserved.
//

import Foundation




class RequestParameters: NSObject {
    
    //MARK: - Nested keys enum
    enum Key: String {
        case user_session
        case start
        case count
        case nick
        case email
        case login
        case password
        case avatar
        case avatarURL
        case category
        case sortable
        case name
        case type_room
        case type_method
        case veg_light_watt
        case reportID
        case weekID
        case report_id
        case week_id
        case comment_id
        case comment_id_reply
        case type
        case session
        case user
        case text
        case parent_id
    }
    
    
    enum ParamType: String {
        case query //get, post body params
        case path //param in path, ...com/report/95 for example
    }
    
    
    
    
    //MARK: - Properties
    var request: Request?
    private var queryParams: [String:Any] = [:]
    private var pathParams: [String:Any] = [:]
    
    

    //MARK: - Lifecycle
    convenience init(_ params: [RequestParameters.Key: Any]? = nil) {
        self.init(request: nil, params)
    }
    
    
    init(request: Request?, _ params: [RequestParameters.Key: Any]? = nil) {
        self.request = request
        super.init()
        
        self.add(params)
    }
    
    
    var dictionary: [String:Any] {
        return self.queryParams
    }
    
    
    
    
    //MARK: - Interface
    
    //convinience
    subscript(key: String) -> Any? {
        get {
            return self[key, .query]
        }
        set {
            self[key, .query] = newValue
        }
    }
    
    
    //convenience
    subscript(key: RequestParameters.Key) -> Any? {
        get {
            return self[key.rawValue, .query]
        }
        set {
            self[key.rawValue, .query] = newValue
        }
    }
    
    
    //convenience
    subscript(key: RequestParameters.Key, type: ParamType) -> Any? {
        get {
            return self[key.rawValue, type]
        }
        set {
            self[key.rawValue, type] = newValue
        }
    }
    
    
    //designated
    subscript(key: String, type: ParamType) -> Any? {
        get {
            var result: Any?
            self.dict(forType: type) { (dict: inout [String : Any]) in
                result = dict[key]
            }
            return result
        }
        set {
            self.dict(forType: type) { (dict) in
                if newValue == nil {
                    _ = dict.removeValue(forKey: key)
                }
                else {
                    dict[key] = newValue
                }
            }
        }
    }
    
    

    
    
    func add(_ params: [String:Any]? = nil) {
        for (key, value) in params ?? [:] {
            self[key] = value
        }
    }
    
    func add(_ params: [RequestParameters.Key: Any]? = nil) {
        for p in params ?? [:] {
            self[p.key.rawValue] = p.value
        }
    }
    
    func add(_ params: (RequestParameters.Key, Any)...) {
        for p in params {
            self[p.0.rawValue] = p.1
        }
    }
    


    
    
    
    var path: String {
        var result = self.request?.pathPattern ?? ""
        if let path = RKPathFromPatternWithObject(result, self.pathParams) {
          result = path
        }
        
        return result
    }

    
    
    
    //MARK: - Routines
    private func dict(forType type: ParamType, _ result: (inout [String:Any]) -> Void) {
        switch type {
        case .query:
            result(&self.queryParams)
            
        case .path:
            result(&self.pathParams)
        }
    }
}
