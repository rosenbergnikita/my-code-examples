//
//  MainMenuViewController.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/8/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import UIKit






class MainMenuViewController : BaseViewController,
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,
UITableViewDelegate,
UITableViewDataSource {
    
    
    
    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var companyLogo: UIImageView!
    @IBOutlet weak var companyName: HFLabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var tabsCollectionView: UICollectionView!
    @IBOutlet weak var tabsCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var rolesHeight: NSLayoutConstraint!
    @IBOutlet weak var rolesCollectionView: UICollectionView!
    private let maxTabCount = 4
    @IBOutlet weak var walletButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var userPhoto: FirstLetter!
    
    
    
    //MARK: - Properties
    private var categoriesByRoles: [[NavigationLevel]] = []
    private var currentCategoryIndex: Int = 0 {
        willSet {
            self.currentCategoryWillChange()
        }
        didSet {
            self.currentCategoryChanged()
        }
    }
    
    
    private var currentCategory: Level? {
        get {
            let c = self.categoriesByRoles[safe: self.currentRoleIndex]
            return c?[safe: self.currentCategoryIndex]
        }
    }
    

    
    private var TabsCollectionViewHeight: CGFloat {
        switch UI.screenSize.width {
        case 320:
            return 50
            
        case 375:
            return 60
            
        case 414:
            return 65
            
        default:
            return 65
        }
    }
    private let RolesHeight = CGFloat(50)
    private var currentRoleIndex: Int = 0 {
        willSet {
            self.currentRoleWillChange()
        }
        didSet {
            self.currentRoleChanged()
        }
    }
    
    private var rolesMenuItems: [MenuType] = []
    private var currentRole: MenuType {
        let t =  self.rolesMenuItems[safe: self.currentRoleIndex]
        return t ?? .PERSONAL
    }
    
    
    var menuType: MenuViewType {
        if let type = DATA.currentCompany?.companyTypeLevel?.menuMini , type {
            return .mini
        }
        else if DATA.currentCompany == nil {
            return .default
        }
        
        return .tabs
    }
    
    let ReuseHeaderID = "section"
    
    var fontForTabs: UIFont {
        switch UI.screenSize.width {
        case 320:
            return UIFont.systemFont(ofSize: 12)
            
        case 375:
            return UIFont.systemFont(ofSize: 14)
            
        case 414:
            return UIFont.systemFont(ofSize: 16)
            
        default:
            return UIFont.systemFont(ofSize: 16)
        }
    }
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.containerView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, UI.settings.menuOffset)
        
        self.tableView.register(MainMenuSectionHeader.self, forHeaderFooterViewReuseIdentifier: ReuseHeaderID)
        self.tableView.register(UINib(nibName: "\(MainMenuSectionHeader.self)", bundle: nil), forHeaderFooterViewReuseIdentifier: ReuseHeaderID)
 
        
        self.fetchData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainMenuViewController.companyChanged), name: NSNotification.Name(rawValue: NK.CURRENT_COMPANY_CHANGED.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainMenuViewController.companyChanged), name: NSNotification.Name(rawValue: NK.CURRENT_COMPANY_LOADED.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainMenuViewController.userChanged), name: NSNotification.Name(rawValue: NK.USER_CHANGED.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainMenuViewController.objectsChanged), name: NSNotification.Name(rawValue: NK.NEW_CHANGE_RECEIVED.rawValue), object: nil)
        
        self.walletButton.imageView?.contentMode = .scaleAspectFit
        self.settingsButton.imageView?.contentMode = .scaleAspectFit
        
        self.loadBusinessMenu()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.usernameLabel.text = DATA.currentUser?.fullName ?? ""
        self.userPhoto.data = DATA.currentUser ?? "AQ"
        self.walletButton.isHidden = DATA.currentUser == nil
        
        
        let c = DATA.currentCompany
        self.companyName.text = c?.name ?? APP_SETTINGS.appName
        if let url = URLFromString(c?.logoURL) {
            self.companyLogo.sd_setImage(with: url)
        }
        else {
            self.companyLogo.image = nil
        }
        
        self.rolesCollectionView.reloadData()
        self.tabsCollectionView.reloadData()
        self.tableView.reloadData()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    
    //MARK: - Data
    override func fetchData() {
        if DATA.currentCompany == nil {
            self.categoriesByRoles = [[DATA.persistent.menuDefault!]]
        }
        else {
            self.categoriesByRoles = [(DATA.currentCompany?.levelsSorted as? [NavigationLevel]) ?? []]
        }
        
        
        self.rolesMenuItems = [.PERSONAL]
        if let user = DATA.currentUser {
            let isPartner = user.inAnyGroup([.DEVELOPER, .PARTNER])
            if isPartner && user.businessMenu != nil {
                self.rolesMenuItems = [.PERSONAL, .BUSINESS]
                self.categoriesByRoles.append([user.businessMenu!])
            }
        }
        
        self.setupUIMenu()
        
        self.currentRoleIndex = 0
        self.rolesCollectionView.reloadData()
        self.rolesCollectionView.collectionViewLayout.invalidateLayout()
        self.currentCategoryIndex = 0
        self.tabsCollectionView.reloadData()
        self.tableView.reloadData()
        
        
    }
    
    
    private func loadBusinessMenu() {
        guard let user = DATA.currentUser, user.inAnyGroup([.DEVELOPER, .PARTNER]) else {
            return
        }
        
        API.businessMenu(onSuccess: {
                self.fetchData()
                }, onFailure:nil)
    }
    
    
    private func category(forSection section: Int) -> NavigationLevel? {
        let role = self.categoriesByRoles[safe: self.currentRoleIndex]
        var category: NavigationLevel?
        switch self.menuType {
        case .default, .tabs:
            category = role?[safe: self.currentCategoryIndex]
        case .mini:
            category = role?[safe: section]
        }
        
        return category
    }
    
    
    private func item(forIndexPath indexPath: IndexPath) -> NavigationLevel? {
        let category = self.category(forSection: indexPath.section)
        let item = category?.sublevelsSorted[indexPath.row]
        return item as? NavigationLevel
    }
    
    
    
    
    //MARK: - UI
    private func setupUIMenu() {
        
        switch self.menuType {
        case .default:
            self.tabsCollectionViewHeight.constant = 0
            self.tabsCollectionView.isHidden = true
            
        case .tabs:
            self.tabsCollectionViewHeight.constant = self.TabsCollectionViewHeight
            self.tabsCollectionView.isHidden = false
            
        case .mini:
            self.tabsCollectionViewHeight.constant = 0
            self.tabsCollectionView.isHidden = true
        }

        
        if let user = DATA.currentUser, user.inAnyGroup([.DEVELOPER, .PARTNER]) {
            self.rolesHeight.constant = self.RolesHeight
            self.rolesCollectionView.isHidden = false
        }
        else {
            self.rolesHeight.constant = 0
            self.rolesCollectionView.isHidden = true

        }
        
    }
    
    
    private func adjustMenuUIRoleChanged() {

        switch self.currentRole {
        case .PERSONAL:
            
            switch self.menuType {
            case .default:
                self.tabsCollectionViewHeight.constant = 0
                self.tabsCollectionView.isHidden = true
                
            case .tabs:
                self.tabsCollectionViewHeight.constant = self.TabsCollectionViewHeight
                self.tabsCollectionView.isHidden = false
                
            case .mini:
                self.tabsCollectionViewHeight.constant = 0
                self.tabsCollectionView.isHidden = true
            }
            self.tableView.backgroundColor = UIColor.clear

            
        case .BUSINESS:
            self.tabsCollectionViewHeight.constant = 0
            self.tabsCollectionView.isHidden = true
            self.tableView.backgroundColor = UI.settings.businessMenuColor
        }
        
        self.rolesCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    
    
    //MARK: - Collection View (Roles/Categories)
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        collectionView.collectionViewLayout.invalidateLayout()
        return 1;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.rolesCollectionView:
            return self.rolesMenuItems.count
            
        case self.tabsCollectionView:
            return self.categoriesByRoles[self.currentRoleIndex].count
            
        default:
            return 0
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case self.rolesCollectionView:
            let reuse = "roleCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuse, for: indexPath) as! MainMenuRoleCell
            
            cell.data = self.rolesMenuItems[safe: indexPath.row]
            cell.isSelected = indexPath.row == self.currentRoleIndex
            return cell
            
            
            
        case self.tabsCollectionView:
            let reuse = "cell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuse, for: indexPath) as! MainMenuCategoryCell
            
            cell.preferredFont = self.fontForTabs
            cell.data = self.categoriesByRoles[self.currentRoleIndex][indexPath.row]
            cell.isSelected = indexPath.row == self.currentCategoryIndex
            
            return cell
            
            
        default:
            return HFCollectionViewCell()
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        switch collectionView {
        case self.rolesCollectionView:
            self.currentRoleIndex = indexPath.row
            
        case self.tabsCollectionView:
            self.currentCategoryIndex = indexPath.row
            
        default:
            break
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let defaultHeight = collectionView.frame.size.height
        let defaultWidth = defaultHeight


        switch collectionView {
        case self.rolesCollectionView:
            let numberOfItems = self.rolesMenuItems.count
            if numberOfItems != 2 {
                switch numberOfItems {
                case 1...4:
                    let width = collectionView.frame.size.width / CGFloat(numberOfItems)
                    return CGSize(width: width, height: defaultHeight)
                    
                default:
                    return CGSize(width: defaultWidth, height: defaultHeight)
                }

            }
            else {
                //Dynamic width changes
                var width = CGFloat(0)
                let baseWidth = collectionView.frame.size.width / CGFloat(numberOfItems)
                if indexPath.row == self.currentRoleIndex {
                    width = baseWidth * 1.2
                }
                else {
                    width = baseWidth * 0.8
                }
                return CGSize(width: width, height: defaultHeight)
            }
            
            
            
        case self.tabsCollectionView:
            let numberOfItems = self.categoriesByRoles[self.currentRoleIndex].count
            switch numberOfItems {
            case 1 ... self.maxTabCount:
                let width = collectionView.frame.size.width / CGFloat(numberOfItems)
                return CGSize(width: width, height: defaultHeight)
                
            default:
                return CGSize(width: defaultWidth, height: defaultHeight)
            }
         
            
            
        default:
            return CGSize.zero
        }
        
    }
    
    
    
    
    //MARK: - Table View (Category Items)
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.currentRole == .BUSINESS {
            return 1
        }
        else {
            switch self.menuType {
            case .default, .tabs:
                return 1

            case .mini:
                return self.categoriesByRoles[self.currentRoleIndex].count
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.category(forSection: section)?.sublevels?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch self.currentRole {
        case .PERSONAL:
            
            
            switch self.menuType {
            case .mini:
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ReuseHeaderID) as? MainMenuSectionHeader
                view?.data = self.category(forSection: section)
                return view
                
            default:
                break
            }
            
        default:
            break
        }
        
        
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let category = self.category(forSection: section)
        switch self.currentRole {
        case .PERSONAL:
            
            switch self.menuType {
            case .mini:
                if let s = category?.name , !s.isEmpty {
                    return 50
                }
                else {
                    return 10
                }
                
            default:
                break
            }
            
        default:
            break
        }
        
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return ZeroView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //let category = self.category(forSection: section)
        switch self.currentRole {
        case .PERSONAL:
            
            switch self.menuType {
            case .mini:
                return 10
                
            default:
                break
            }
            
        default:
            break
        }
        
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuse = "cell"
        var cell: MainMenuItemCell! = tableView.dequeueReusableCell(withIdentifier: reuse) as! MainMenuItemCell
        if cell == nil {
            cell = MainMenuItemCell(style: .default, reuseIdentifier: reuse)
        }
        
        cell.preferredColor = (self.currentRole == .BUSINESS) ? UIColor.white : Color(r: 119, g: 119, b: 119)
        cell.data = self.item(forIndexPath: indexPath)
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let item = self.item(forIndexPath: indexPath) else {
            return
        }
        
        if item.command != nil {
            UI.doServerUICommand(item)
        }
        else {
            if let company = DATA.currentCompany, self.currentRole == .PERSONAL {
                if company.isNeedUpdate {
                    UI.showCompanySplash(withLoading: false)
                    UI.showWaiting()
                    COMPANY_LOGIC.tryToLoadCompany(company, success: {
                        UI.hideWaiting()
                    })
                    return
                }
            }
            

            UI.showData(item, pushToRoot: true)
        }
    }
    
    
    
    
    //MARK: - UI Routines
    func currentCategoryWillChange() {
        let indexPath = IndexPath(item: self.currentCategoryIndex, section: 0)
        let categoryCell = self.tabsCollectionView.cellForItem(at: indexPath) as? MainMenuCategoryCell
        categoryCell?.isSelected = false
    }
    
    func currentCategoryChanged() {
        let indexPath = IndexPath(item: self.currentCategoryIndex, section: 0)
        let categoryCell = self.tabsCollectionView.cellForItem(at: indexPath) as? MainMenuCategoryCell
        categoryCell?.isSelected = true
        
        self.tableView.reloadData()
        //scroll to top
        self.tableView.setContentOffset(CGPoint.zero, animated:false)
    }
    
    
    func currentRoleWillChange() {
        let indexPath = IndexPath(item: self.currentRoleIndex, section: 0)
        let roleCell = self.rolesCollectionView.cellForItem(at: indexPath) as? MainMenuRoleCell
        roleCell?.isSelected = false
    }
    
    
    func currentRoleChanged() {
        let indexPath = IndexPath(item: self.currentRoleIndex, section: 0)
        let roleCell = self.rolesCollectionView.cellForItem(at: indexPath) as? MainMenuRoleCell
        roleCell?.isSelected = true
        
        
        self.adjustMenuUIRoleChanged()
        
        
        self.currentCategoryIndex = 0
        self.currentCategoryChanged()
    }
    
    
    
    
    
    //MARK: - Notifications
    func companyChanged() {
        self.fetchData()
    }
    
    
    func userChanged() {
        self.fetchData()
        self.loadBusinessMenu()
    }
    
    
    func objectsChanged() {
        self.fetchData()
    }
    
    
    
    //MARK: - Actions
    @IBAction func settingsAction(_ sender: Any) {
        UI.doCommand(.showSettings)
    }
    
    
    @IBAction func walletAction(_ sender: Any) {
        UI.push(CreateVC() as MarketingContainerViewController)
    }
    
    
    @IBAction func profileAction(_ sender: Any) {
        //need to remake
        RootVC()?.doServerUICommand(RootViewController.Command(onlyAuth: true, command: "OPEN_PROFILE"))
    }
}
