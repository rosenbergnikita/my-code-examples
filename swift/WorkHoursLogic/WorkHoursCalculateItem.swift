//
//  WorkHoursCalculateItem.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/25/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation


struct WorkHoursCalculateItem:
CustomDebugStringConvertible, CustomStringConvertible {
    var open: Date
    var close: Date
    var ownIndex: Int
    
    
    public var description: String {
        return self.debugDescription
    }
    
    
    public var debugDescription: String {
        return "\(self.open) \(self.close)"
    }
}
