//
//  WeekDayWorkHoursValue.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/19/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation




class WeekDayWorkHoursValue: WorkHoursValue {
    
    //MARK: Properties
    var dayFrom: String?
    var dayTo: String?
    override var day: String {
        var result = ""
        var delimiter = ""
        if let from = self.dayFrom , !from.isEmpty {
            result += from
            delimiter = " - "
        }
        if let to = self.dayTo , !to.isEmpty {
            result += "\(delimiter)\(to)"
        }
        
        return result
    }
    
    

    
    
    
    //MARK: - Lifecycle
    override internal func populate() {
        super.populate()
        
        if self.everyDaySameTime {
            self.populateEveryDaySameTimeTrue()
        }
        else {
            self.populateEveryDaySameTimeFalse()
        }
        
        
    }
    
    
    private func populateEveryDaySameTimeTrue() {
        guard let field = self.field as? LsValsField else {
            return
        }
        
        self.editLsVals(field: field)
        self.dayFrom = field.compiledTextValueSingle()
        
        let united = field.unitedFieldsSorted
        for subfield in united {
            if let dayToField = subfield as? LsValsField {
                self.editLsVals(field: dayToField)
                self.dayTo = dayToField.compiledTextValueSingle
            }
            else if let onme = subfield as? OnMeField {
                for subfieldGroup in onme.subfieldsSorted {
                    self.populateHoursData(from: subfieldGroup.subfieldsSorted)
                }
            }
        }
    }
    
    
    private func populateEveryDaySameTimeFalse() {
        let subfields = self.field.subfieldsSorted
        if subfields.count != 2 {
            return
        }
        
        for subfield in subfields {
            if let dayFromField = subfield as? LsValsField {
                self.editLsVals(field: dayFromField)
                self.dayFrom = dayFromField.compiledTextValueSingle
            }
            else if let onme = subfield as? OnMeField {
                for subfieldGroup in onme.subfieldsSorted {
                    self.populateHoursData(from: subfieldGroup.subfieldsSorted)
                }
            }
        }
    }
    
    
    
    
    //MARK: - Interface
    override func datetimeRepresentationToCheck(_ checkDate: Date) -> [WorkHoursCalculateItem] {
        let cal = self.company.calendar
        
        //Make array of weekdays [sunday, monday, ..] with current locale
        var weekDaysTmp = self.company.dateFormatter.weekdaySymbols
        var weekDays: [String] = []
        weekDays.insert("", at: 0) //index based on 1
        for i: Int in 0 ..< weekDaysTmp!.count {
            weekDays.insert(weekDaysTmp![i].lowercased(), at:i+1)
        }
        
        //Calculate weekdays edges, e.g, Mon-Wed it's a 2-3
        var weekdayFromIndex: Int = 0
        var weekdayToIndex: Int = 0
        if let from = self.dayFrom , from.isEmpty == false {
            weekdayFromIndex = weekDays.index(of: from.lowercased()) ?? 0
        }
        if let to = self.dayTo , to.isEmpty == false {
            weekdayToIndex = weekDays.index(of: to.lowercased()) ?? 0
        }

        //Calculate days between edges, e.g. tue, wed, thu between mon and fri
        var weekDaysToCheck: [Int] = []
        if weekdayFromIndex > 0 {
            weekDaysToCheck.append(weekdayFromIndex)
        }
        if weekdayToIndex > 0 {
            weekDaysToCheck.append(weekdayToIndex)
        }
        if weekdayFromIndex > 0
            && weekdayToIndex > 0
            && (weekdayFromIndex-weekdayToIndex) != 0 {
            // Monday - Friday
            if ( weekdayFromIndex <= weekdayToIndex ) {
                for i in stride(from: weekdayFromIndex+1, through: weekdayToIndex-1, by: 1) {
                //for i: Int in weekdayFromIndex+1 ... weekdayToIndex-1 {
                    weekDaysToCheck.append(i);
                }
            }
            //Friday - Monday
            else {
                //from start index to saturday1
                for i: Int in weekdayFromIndex+1 ... 7 {
                    weekDaysToCheck.append(i)
                }
                //from sunday to end index
                let to = weekdayToIndex-1
                if to >= 1 {
                    for i: Int in 1 ... to {
                        weekDaysToCheck.append(i)
                    }
                }
            }
        }
        
        //Find current weekday
        let c = cal.dateComponents([.weekday], from: checkDate)
        let currentWeekDay = c.weekday
        
        //Check calculated weekdays and calculate datetimes to next check
        var datesToCheck: [WorkHoursCalculateItem] = []
        
        for weekDayIndex in weekDaysToCheck {
            var closestDates: [Date] = []
            
            //Find weekday concrete date in closest future, (e.g. next Monday is 17 may)
            //hours and minutes doesn't matter, because will be overwritten
            var c = DateComponents()
            c.weekday = weekDayIndex
            if let computedDate = cal.nextDate(after: checkDate, matching: c, matchingPolicy: .nextTime, repeatedTimePolicy: .first, direction: .forward) {
                closestDates.append(computedDate)
            }
            
            //closes past same weekday
            if let computedDate = cal.nextDate(after: checkDate, matching: c, matchingPolicy: .nextTime, repeatedTimePolicy: .first, direction: .backward) {
                closestDates.append(computedDate)
            }
            
            //Checks current day
            if weekDayIndex == currentWeekDay {
                closestDates.append(checkDate)
            }
            
            let datesToCheckTmp = self.translateDays(closestDates)
            datesToCheck.append(contentsOf: datesToCheckTmp)
        }
        
        return datesToCheck
    }
    
}
