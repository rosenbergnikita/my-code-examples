//
//  ConcreteDayWorkHoursValue.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/19/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation



class ConcreteDayWorkHoursValue: WorkHoursValue {
    
    //MARK: Properties
    var dateFrom: Date?
    var dateTo: Date?
    private var dateFormatPresentation: String! = "dd.MM.yyyy"
    override var day: String {
        var result = ""
        var delimiter = ""
        if let from = self.dateFrom {
            result += self.company.stringFromDate(from, format: self.dateFormatPresentation)
            delimiter = " - "
        }
        if let to = self.dateTo {
            result += delimiter
            result += self.company.stringFromDate(to, format: self.dateFormatPresentation)
        }
        
        return result
    }
    
    
    
    
    //MARK: - Lifecycle
    override internal func populate() {
        super.populate()
        
        if self.everyDaySameTime {
            self.populateEveryDaySameTimeTrue()
        }
        else {
            self.populateEveryDaySameTimeFalse()
        }
        
        
    }
    
    
    private func populateEveryDaySameTimeTrue() {
        let subfields = self.field.subfieldsSorted
        if subfields.count != 3 {
            return
        }
        
        var dateCandidates: [Date] = []
        
        for subfield in subfields {
            if let dateFromField = subfield as? DateField {
                self.dateFormatPresentation = dateFromField.printFormat
                if let d = self.company.dateFromString(dateFromField.compiledTextValueSingle, format: "dd.MM.yyyy") {
                    dateCandidates.append(d)
                }
            }
            else if let onme = subfield as? OnMeField {
                for subfieldGroup in onme.subfieldsSorted {
                    self.populateHoursData(from: subfieldGroup.subfieldsSorted)
                }
            }
            
            
        }
        
        
        //sort asc
        dateCandidates.sort(by: { $0.compare($1) == ComparisonResult.orderedAscending })
        if dateCandidates.count == 2 {
            self.dateFrom = dateCandidates[0]
            self.dateTo = dateCandidates[1]
        }
        else if dateCandidates.count == 1 {
            self.dateFrom = dateCandidates[0]
        }
        
        
    }
    
    
    private func populateEveryDaySameTimeFalse() {
        let subfields = self.field.subfieldsSorted
        if subfields.count != 2 {
            return
        }
        
        for subfield in subfields {
            if let dateFromField = subfield as? DateField {
                self.dateFormatPresentation = dateFromField.printFormat
                self.dateFrom = self.company.dateFromString(dateFromField.compiledTextValueSingle, format: "dd.MM.yyyy")
            }
            else if let onme = subfield as? OnMeField {
                for subfieldGroup in onme.subfieldsSorted {
                    self.populateHoursData(from: subfieldGroup.subfieldsSorted)
                }
            }
        }
    }
    
    
    
    
    //MARK: - Interface
    override func datetimeRepresentationToCheck(_ checkDate: Date) -> [WorkHoursCalculateItem] {
        let cal = self.company.calendar
        
        //Calculate edges
        var daysToCheck: [Date] = []
        
        if self.dateFrom != nil && self.dateTo != nil {
            var next = self.dateFrom!
            let end = self.dateTo!
            daysToCheck.append(next)
            
            if cal.isDate(next, inSameDayAs: end) {
                //daysToCheck.append(end)
            }
            else {
                repeat {
                    if let n = (cal as NSCalendar).date(byAdding: .day, value: 1, to: next, options: NSCalendar.Options(rawValue: 0)) {
                        next = n
                        daysToCheck.append(next)
                    }
                }
                    while cal.isDate(next, inSameDayAs: end) == false
            }
        }
        else if self.dateFrom != nil {
            daysToCheck.append(self.dateFrom!)
        }
        else if self.dateTo != nil {
            daysToCheck.append(self.dateTo!)
        }
        
        
        //Check calculated weekdays and calculate datetimes to next check
        let datesToCheck = self.translateDays(daysToCheck)
        return datesToCheck
    }
    
}
