//
//  WorkHoursLogic.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/16/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import UIKit






class WorkHoursLogic:
CustomDebugStringConvertible, CustomStringConvertible {
    
    //MARK: - Properties
    internal var field: FieldBase
    internal var company: ObjectInCompany
    internal var values: [WorkHoursValue] = []
    
    
    
    
    //MARK: - Lifecycle
    init(field: FieldBase, company: ObjectInCompany) {
        //assert(field.isTimeWorkDeep, "Field isn't IS_TIME_WORK")
        self.field = field
        self.company = company
        self.translateValues()
    }
    
    
    
  
    //MARK: - Calculations
    private func translateValues() {
        
        let united = self.field.unitedFieldsSorted
        let subfields = self.field.subfieldsSorted
        
        if united.count == 2 {
            //Days of the week (every day the same time)
            self.values.append(WeekDayWorkHoursValue(field: self.field, company: self.company, everyDaySameTime: true))
        }
        else {
            for subfieldGroup in subfields {
                let subfieldGroupSubfields = subfieldGroup.subfieldsSorted
                if subfieldGroupSubfields.count == 3 {
                    //Date (every day the same time)
                    self.values.append(ConcreteDayWorkHoursValue(field: subfieldGroup, company: self.company, everyDaySameTime: true))
                }
                else {
                    for subfield in subfieldGroupSubfields {
                        if subfield is LsValsField {
                            //Days of the week (every day various time)
                            self.values.append(WeekDayWorkHoursValue(field: subfieldGroup, company: self.company, everyDaySameTime: false))
                        }
                        else if subfield is DateField {
                            //Date (every day various time)
                            self.values.append(ConcreteDayWorkHoursValue(field: subfieldGroup, company: self.company, everyDaySameTime: false))
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    //MARK: - Interface
    var currentOpennessState: OpennessState {
        let result:(state: OpennessState, dataIndex: IndexPath?, nextEventDate: Date? ) = self.currentOpennessStateDetailed()
        return result.state
    }
 
    
    func currentOpennessStateDetailed() -> (state: OpennessState, dataIndex: IndexPath?, nextEventDate: Date?) {
        var resultIndexPath: IndexPath? = nil
        var resultDate: Date? = nil
        var intervalToNextEvent: TimeInterval = DBL_MAX
        var resultState: OpennessState = .unknown
        
        let now = self.company.localTime
        
        // Find closes event and it's state;
        for i: Int in 0 ..< self.values.count {
            let value = self.values[i]
            let info = value.opennessStateDetailed(now)
            
            if let eventDate = info.nextEventDate {
                let interval = eventDate.timeIntervalSince(now)
                if interval > 0 && interval < intervalToNextEvent {
                    intervalToNextEvent = interval
                    resultState = info.state
                    resultIndexPath = IndexPath(row: info.dataIndex!, section: i)
                    resultDate = info.nextEventDate
                }
            }
        }
        
        return (resultState, resultIndexPath, resultDate)
    }
    
    
    
    var currentOpenessStateString: NSAttributedString {
        let result = NSMutableAttributedString()
        
        switch self.currentOpennessState {
        case .open:
            result.mutableString.setString(Localized("field_description_open"))
            result.addAttribute(NSForegroundColorAttributeName, value: UI.settings.openColor, range: NSMakeRange(0, result.length))
            
        case .close:
            result.mutableString.setString(Localized("field_description_close"))
            result.addAttribute(NSForegroundColorAttributeName, value: UI.settings.closeColor, range: NSMakeRange(0, result.length))
            
        default:
            break
        }
        
        return result
    }
    
    
    var openessHint: NSAttributedString {
        let now = self.company.localTime
        let info = self.currentOpennessStateDetailed()
        
        let result = NSMutableAttributedString()
        let state = info.state
        
        guard let eventDate = info.nextEventDate else {
            return result
        }
        
        
        // Get conversion to days, hours, minutes
        let breakdownInfo = (self.company.calendar as NSCalendar).components([.minute, .hour, .day], from: now as Date, to: eventDate, options: NSCalendar.Options(rawValue: 0))
        var text = ""
        if breakdownInfo.day! >= 1 {
            text += " " + LocalizedPlural(NSNumber(value: breakdownInfo.day!), textCode: "plurals_chat_last_seen_day")
        }
        if breakdownInfo.hour! >= 1 {
            text += " " + LocalizedPlural(NSNumber(value: breakdownInfo.hour!), textCode: "plurals_chat_last_seen_hour")
        }
        if breakdownInfo.minute! >= 1 {
            text += " " + LocalizedPlural(NSNumber(value: breakdownInfo.minute!), textCode: "plurals_chat_last_seen_minute")
        }
        
        switch state {
        case .open:
            result.mutableString.setString(Localized("field_description_close_at")+text)
            result.addAttribute(NSForegroundColorAttributeName, value: UI.settings.closeColor, range: NSMakeRange(0, result.length))
            
        case .close:
            result.mutableString.setString(Localized("field_description_open_at")+text)
            result.addAttribute(NSForegroundColorAttributeName, value: UI.settings.openColor, range: NSMakeRange(0, result.length))
            
        default:
            break
        }
        
        return result
    }
    
    
    func hoursData(for index: IndexPath) -> WorkHoursDataItem? {
        guard let section = self.values[safe: index.section] else {
            return nil
        }
        
        guard let row = section.hoursData[safe: index.row] else {
            return nil
        }
        
        return row
    }
    
    
    
    
    //MARK: - UI Routines
    func drawTo(container hoursContainer: UIView) {
        let workHours = self
        
        let viewHeight = CGFloat(21)
        var hoursContainerTotalHeight = CGFloat(0)
        
        for s in hoursContainer.subviews {
            s.removeFromSuperview()
        }
        
        let opennessInfo = workHours.currentOpennessStateDetailed()
        let indexPathToColor = opennessInfo.dataIndex
        let openState = opennessInfo.state
        
        for i: Int in 0 ..< workHours.values.count {
            let value = workHours.values[i]
            
            for j: Int  in 0 ..< value.hoursData.count {
                let periodData = value.hoursData[j]
                let view = CreateView() as WorkHoursView
                view.translatesAutoresizingMaskIntoConstraints = false
                hoursContainer.addSubview(view)
                view.data = periodData.toDictionay() as [String : Any]?
                
                if j == 0 {
                    view.day.isHidden = false
                    view.day.text = value.day
                    if indexPathToColor?.section == i && openState == .some(.open) {
                        view.day.textColor = UI.settings.openColor
                    }
                }
                else {
                    view.day.isHidden = true
                }
                
                view.uiStateCurrentOpened = indexPathToColor?.section == i && indexPathToColor?.row == j && openState == .some(.open)
                
                // Top margin
                hoursContainer.addConstraint(
                    NSLayoutConstraint(item: view,
                                       attribute: .top,
                                       relatedBy: .equal,
                                       toItem: hoursContainer,
                                       attribute: .top,
                                       multiplier: 1,
                                       constant: hoursContainerTotalHeight))
                
                // Height constraint
                hoursContainer.addConstraint(
                    NSLayoutConstraint(item: view,
                                       attribute: .height,
                                       relatedBy: .equal,
                                       toItem: nil,
                                       attribute: .notAnAttribute,
                                       multiplier: 1,
                                       constant: viewHeight))
                
                // Width constraint
                hoursContainer.addConstraint(
                    NSLayoutConstraint(item: hoursContainer,
                                       attribute: .width,
                                       relatedBy: .equal,
                                       toItem: view,
                                       attribute: .width,
                                       multiplier: 1,
                                       constant: 0))
                
                // Center horizontally
                hoursContainer.addConstraint(
                    NSLayoutConstraint(item: hoursContainer,
                                       attribute: .centerX,
                                       relatedBy: .equal,
                                       toItem: view,
                                       attribute: .centerX,
                                       multiplier: 1,
                                       constant: 0))
                
                if i == workHours.values.count-1 && j == value.hoursData.count-1 {
                    // Bottom margin
                    hoursContainer.addConstraint(
                        NSLayoutConstraint(item: view,
                                           attribute: .bottom,
                                           relatedBy: .equal,
                                           toItem: hoursContainer,
                                           attribute: .bottom,
                                           multiplier: 1,
                                           constant: 0))
                }
                
                hoursContainerTotalHeight += viewHeight
            }
            
            
        }

    }
    
    
    
    
    //MARK: - Debug
    public var description: String {
        return self.debugDescription
    }
    
    
    public var debugDescription: String {
        var result = ""
        for v in self.values {
            result.append(v.description + "\n\n")
        }
        return result
    }
}


