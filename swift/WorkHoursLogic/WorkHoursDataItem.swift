//
//  WorkHoursItem.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/19/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation



struct WorkHoursDataItem {
    //MARK: Properties
    var eventName: String = ""
    var begin: String = ""
    var end: String = ""
    var price: String = ""
    
    func toDictionay() -> [String: String] {
        let d = ["eventName":self.eventName,
                 "begin":   self.begin,
                 "end":     self.end,
                 "price":   self.price]
        return d
    }
}