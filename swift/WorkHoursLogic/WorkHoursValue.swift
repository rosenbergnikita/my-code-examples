//
//  WorkHoursValue.swift
//  HotelFace
//
//  Created by Nikita Rosenberg on 7/19/16.
//  Copyright © 2016 Hotel-Face. All rights reserved.
//

import Foundation


class WorkHoursValue {
    
    //MARK: Properties
    var field: FieldBase
    internal var company: ObjectInCompany
    var hoursData: [WorkHoursDataItem] = []
    var everyDaySameTime: Bool = false
    private(set) var day: String = "" //Text information of day (period)
    
    
    
    //MARK: - Lifecycle
    required init(field: FieldBase, company: ObjectInCompany, everyDaySameTime: Bool) {
        self.field = field
        self.company = company
        self.everyDaySameTime = everyDaySameTime
        self.populate()
    }
    
    internal func populate() {
        
    }
    
    
    
    //MARK: - Interface
    func datetimeRepresentationToCheck(_ checkDate: Date) -> [WorkHoursCalculateItem] {
        return []
    }
    
    
    func opennessStateDetailed(_ checkDate: Date) -> (state: OpennessState, dataIndex: Int?, nextEventDate: Date?) {
        //result variables
        var resultNextEventDate: Date? = nil
        var intervalToNextEventDate: TimeInterval = DBL_MAX
        var resultHoursIndex = -1
        var resultState: OpennessState = .unknown
        
        //Final checks of calculated dates
        let datesToCheck = self.datetimeRepresentationToCheck(checkDate)
        for date in datesToCheck {
            //now we have two concrete dates to compare with checkDate
            //e.g. monday 17 may 14:00
            //and  monday 18 may 01:00
            
            // check date between dates
            let c1 = checkDate.compare(date.open as Date)
            let c2 = checkDate.compare(date.close as Date)
            if (c1 == .orderedDescending || c1 == .orderedSame)
                &&
                (c2 == .orderedAscending && resultState != .open) {
                // checkDate is between open and close
                resultNextEventDate = date.close
                resultState = .open
                resultHoursIndex = date.ownIndex
                break
            }
            else if resultState != .some(.open) {
                //checkDate is outside range
                resultState = .close
                let timeToOpen = date.open.timeIntervalSince(checkDate)
                if timeToOpen < intervalToNextEventDate && timeToOpen > 0 {
                    intervalToNextEventDate = timeToOpen
                    resultNextEventDate = date.open
                    resultHoursIndex = date.ownIndex
                }
            }
        }
        
        return (resultState, resultHoursIndex, resultNextEventDate)
    }

    
    
    //MARK: - Internal
    internal func translateDays(_ daysToCheck: [Date]) -> [WorkHoursCalculateItem] {
        let cal = self.company.calendar
        var datesToCheck: [WorkHoursCalculateItem] = []
        
        for computedDate in daysToCheck {
            for i: Int in 0 ..< self.hoursData.count {
                let hourData = self.hoursData[i]
                
                let openString = NonEmpty(hourData.begin, defaultReturn: "00:00")
                let closeString = NonEmpty(hourData.end, defaultReturn: "23:59")
                
                var open = DateComponents()
                open.hour = 0
                open.minute = 0
                var close = DateComponents()
                close.hour = 23
                close.minute = 59
                if let d = self.company.dateFromString(openString, format: "HH:mm") {
                    open = (cal as NSCalendar).components([.hour,.minute], from: d)
                }
                if let d = self.company.dateFromString(closeString, format: "HH:mm") {
                    close = (cal as NSCalendar).components([.hour,.minute], from: d)
                }
                
                let openMinutes = open.hour!*60 + open.minute!
                let closeMinutes = close.hour!*60 + close.minute!
                
                var components = (cal as NSCalendar).components(NSCalendar.Unit(rawValue: UInt.max), from: computedDate)
                components.hour = open.hour
                components.minute = open.minute
                let openDate = cal.date(from: components)
                components.hour = close.hour
                components.minute = close.minute
                var closeDate = cal.date(from: components)
                
                //12:01 - 15:00
                if openMinutes < closeMinutes {
                    
                }
                    //23:00 - 02:30
                else {
                    var addOneDay = DateComponents()
                    addOneDay.day = 1
                    closeDate = (cal as NSCalendar).date(byAdding: addOneDay, to: closeDate!, options: NSCalendar.Options(rawValue: 0))
                }
                
                let dateData = WorkHoursCalculateItem(open: openDate!, close: closeDate!, ownIndex: i)
                datesToCheck.append(dateData)
            }
        }
        
        return datesToCheck
    }
    
    
    internal func populateHoursData(from fields: [FieldBase]) {
        var hoursItem = WorkHoursDataItem()
        for subfield in fields {
            switch subfield {
            case is ListField:
                hoursItem.eventName = subfield.compiledTextValueSingle()
                
            case is LsValsField:
                hoursItem.price = subfield.compiledTextValueSingle
                
            case is DiscountField:
                hoursItem.price = "\(subfield.compiledTextValueSingle)%"
                
            case is TimeField:
                if let openClose = APPOINTMENT_KEY(rawValue: subfield.appointment ?? "")  {
                    switch openClose {
                    case .OPEN:
                        hoursItem.begin = subfield.compiledTextValueSingle
                    case .CLOSE:
                        hoursItem.end = subfield.compiledTextValueSingle
                    }
                }
                else {
                    if hoursItem.begin.isEmpty {
                        hoursItem.begin = subfield.compiledTextValueSingle
                    }
                    else if hoursItem.end.isEmpty {
                        hoursItem.end = subfield.compiledTextValueSingle
                    }
                }
                
            default:
                break
            }
        }
        self.hoursData.append(hoursItem)
    }

    
    internal func editLsVals(field: LsValsField) {
        guard  let value = field.valuesSorted.first else {
            return
        }
        
        let valueText = value.compiledTextValue ?? ""
        var newValue = valueText
        
        //Make array of weekdays [sunday, monday, ..] with current locale
        let weekDaysTmp = self.company.dateFormatter.weekdaySymbols
        switch valueText {
        case "153", "154", "155", "156", "157", "158":
            let index = Int(valueText)! - 152
            newValue = weekDaysTmp?[safe: index] ?? valueText
            
        case "159":
            newValue = weekDaysTmp?[safe: 0] ?? valueText
            
        default:
            break
        }
        
        if value.compiledTextValue != newValue {
            value.compiledTextValue = newValue
        }
    }
    
    
    //MARK: - Debug
    public var description: String {
        return self.debugDescription
    }
    
    
    public var debugDescription: String {
        let datesToCheck = self.datetimeRepresentationToCheck(Date())
        var result = self.day + "\n"
        for date in datesToCheck {
            result.append(date.description + "\n")
        }
        return result
    }
}
